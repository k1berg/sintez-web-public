
//using Sintez.IO;

// Test example, from stellarium
// 21-10-2020 22:41:20 +3 60N 30E 
// aAnd J2000=(00-08-23.48, 29-05-22.1) JNow=(00-09-29.14, 29-12-26.1) (2.37141667, 29.20725)deg (0.0065872685, 0.08113125)rev
// EQ2=(23-34-40.42, 29-12-26.1) (earth) AzAlt=(169-25-18.6, +58+22+28.4) (353.6684167 deg, 0.9824122685rev)
// MST=23-44-10.7 VST=23-44-9.6
// JD=2459144,320372 MJD=59143,820372

using System;
using Sintez.Geometry;
namespace ASCOM.Sintez {

	// ��������!!!
	// ��� ���� ���������� � ��������!!! (� �� ����� ���� ������ 1 ��� ������ -1)
	// �������� - � ��������(!!!) � �������
	// ������� � ������� � ���� - � �������� ������ ��������
	public class SintezHelper {
		public double lat, lon, elev, timeOfs;
		private DateTimeOffset TestDate =  new DateTimeOffset(2020, 10-1, 21, 22, 41, 20, TimeSpan.FromHours(3));
		private bool fUseTestDate = false;

		public void SetTestDate(DateTimeOffset newDT) {
			TestDate = newDT;
			if (TestDate != null) fUseTestDate = true;
			else fUseTestDate = false;
		}
		public void CancelTestDate() {
			fUseTestDate = false;
		}
		public DateTimeOffset getTime() {
			if (fUseTestDate && TestDate!=null) return TestDate;
			else return DateTimeOffset.Now;
		}
		public double getTimeSec() {
			return getTime().Ticks / 10000000.0;
		}
		public Point convertCoordsSkyToEarth(Point coords) {
			return convertCoordsSkyToEarth(coords.x, coords.y);
		}
		public Point convertCoordsEarthToSky(double ra2, double dec2) {
			return convertCoordsSkyToEarth(ra2, dec2);
		}
		public Point convertCoordsEarthToSky(Point coords) {
			return convertCoordsEarthToSky(coords.x, coords.y);
		}
		public double convertAlphaToEarth(double ra1) {
			// ��� ������ ra1 �� �������� � ������� (��� ����?)
			ra1 = ra1 * 360.0;
			double izone = 3;
			//Current time
			DateTimeOffset runTime = getTime();
			/*int ye = runTime.Year;
			if (ye < 1000) ye += 1900;
			int mo = runTime.Month;
			int da = runTime.Day;
			int ho = runTime.Hour;
			int mi = runTime.Minute;
			int se = runTime.Second;*/
			//Universal time
			izone = runTime.Offset.TotalHours; // � ��� ���� ��� �� �����?

			double d_jd = runTime.Ticks/1e7 / 86400.0 + 1721425.5 - 0.000005 - 13.0/(360*3600); // ������ ���� 1721424.5, �� ��� �� 1 ����
			//double d_da = da + (ho + mi / 60.0 + se / 3600.0) / 24.0;
			//double d_jd = cal_jd(d_da, mo, ye);
			//double d_deltajd = cal_jd(1, 1, 1); // ������ � 2 ���?!

			d_jd = d_jd - izone / 24.0;
			//Console.WriteLine("Current corrected JD=" + d_jd.ToString());
			double d_mjd = d_jd - 2415020;

			double stt = cal_stime(d_mjd, lon);

			double tun = stt - ra1;  // � ������ �������� 353.6684167
			if (tun < 0.0) tun += 360.0;

			tun /= 360.0; // ������� � �������
			// � �������� +/- 0.5
			if (tun >= 0.5) tun -= (int)(tun + 0.5);
			if (tun < -0.5) tun -= (int)(tun - 0.5);
			return tun;
		}

		public Point convertCoordsSkyToEarth(double ra1, double dec1) {
			return new Point(convertAlphaToEarth(ra1), dec1);
		}

		public Point convertToAltAz(double ra1, double dec1) {
			const double kpi = Math.PI * 2;
			double tun = convertAlphaToEarth(ra1);
			tun *= kpi; dec1 *= kpi;
			double zd = horcz(dec1, lat*(kpi/360), tun);
			double az = horca(dec1, lat * (kpi / 360), tun, Math.PI / 2.0 - zd);
			return new Point(az / kpi, zd / kpi);
		}

		private double horcz(double d, double fi, double tu)  // d, tungle, FI in radians
		{
			double z;
			z = Math.Sin(d) * Math.Sin(fi) + Math.Cos(d) * Math.Cos(fi) * Math.Cos(tu);
			z = Math.PI / 2.0 - Math.Acos(z);
			return (z);  // z in radians
		}

		private double horca(double d, double fi, double tu, double z)  // d, tungle, FI, h in radians
		{
			//sin(A) = cos(d)*sin(t)/sin(z) 
			//sin(z)*cos(A) = sin(j)*cos(d)*cos(t) - cos(j)*sin(d)
			double a = (Math.Sin(fi)*Math.Cos(d)*Math.Cos(tu) - Math.Cos(fi)*Math.Sin(d)) / Math.Sin(z);
			a = Math.Acos(a);
			if (Math.Cos(d) * Math.Sin(tu) / Math.Sin(z) < 0.0) a = 2.0 * Math.PI - a;
			a = a - Math.PI;
			if (a < 0) a += 2.0 * Math.PI;
			return (a);
		}

		public double cal_jd(double d_day, int l_mon, int l_year) {
			int l_day = (int)d_day;
			double d_par = d_day - l_day;
			const int ig = 15 + 31 * (10 + 12 * 1582);
			int mtmp = l_mon + 1;
			int ytmp = l_year;
			if (l_mon <= 2) {
				mtmp += 12;
				ytmp--;
			}
			int res = (int)(365.25 * ytmp + 30.6001 * mtmp + l_day + 1720995);
			if (l_day + 31 * (l_mon + 12 * l_year) >= ig) {
				int tmp = ytmp / 100;
				res = res + 2 - tmp + tmp / 4;
			}
			return (res + d_par - 0.5);
		}

		public int mjd_dow(double mjd) {
			int dow = ((((int)(mjd - 0.5)) + 1) % 7); /* 1/1/1900 (mjd 0.5) is a Monday*/
			if (dow < 0) dow += 7;
			return (dow);
		}

		public double mjd_cal(double mjd, out double day, out double m, out double y) {
			double d = mjd + 0.5;
			int i = (int)d;
			double f = d - i;
			if (f >= 1) {
				f -= 1;
				i += 1;
			}
			if (i > -115860.0) {
				int a = (int)((i / 36524.25) + .99835726) + 14;
				i += 1 + a - (a / 4);
			}
			int b = (int)((i / 365.25) + .802601);
			int ce = i - (int)((365.25 * b) + .750001) + 416;
			int g = (int)(ce / 30.6001);
			int mn = g - 1;
			double dy = ce - (int)(30.6001 * g) + f;
			int yr = b + 1899;
			if (g > 13.5) mn = g - 13;
			if (mn < 2.5) yr = b + 1900;
			if (yr < 1) yr -= 1;

			day = dy;
			m = mn;
			y = yr;
			return mjd;
		}

		public double cal_stime(double day, double lam) {
			double t = day - 0.5;
			double a = (12.1128 - 0.052954 * t) / 180.0 * Math.PI;
			double b = (280.0812 + 0.985647 * t) / 180.0 * Math.PI;
			double c = (64.3824 + 13.176398 * t) / 180.0 * Math.PI;
			double teta = 100.075542 + 360.985647348 * t + 0.29e-12 * t * t
				- 4.392e-3 * Math.Sin(a) + 0.053e-3 * Math.Sin(2.0 * a)
				- 0.325e-3 * Math.Sin(2.0 * b) - 0.05e-3 * Math.Sin(2.0 * c) + lam;
			teta = teta + 26.0 * 15.0 / 3600.0;
			int nob = (int)Math.Floor(teta / 360.0);
			return (teta - nob * 360);
		}
	}
}
