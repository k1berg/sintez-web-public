using System;
using System.Collections;
using System.Collections.Generic;
//using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ASCOMCore;
using ASCOM.DeviceInterface;
using Sintez;
using Sintez.Utils.Log;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Sintez.Alpaca {

	[ApiController]
	[ApiExplorerSettings(IgnoreApi = false)]
	[Route("service/telescope/")]
	//[Route("api/v1/telescope/{device_number}/")]
	public class ServiceTelescopeController : Controller {
		public static Sintez.IO.TelescopeController Telescope { get; set; }
		public ServiceTelescopeController() {
		}
		[HttpGet("visiblebounds")]
		public ActionResult<PointListResponse> VisibleBounds(int ClientID, int ClientTransactionID) {
			PointListResponse resp = new PointListResponse(ClientID, ClientTransactionID, "VisibleBounds", null);
			try {
				resp.Value = Telescope.VisibleBounds;
			} catch (Exception e) {
				resp.ErrorNumber = ASCOM.ErrorCodes.DriverBase+1;
				resp.ErrorMessage = e.Message;
			}
			return resp;
		}
		[HttpGet("motionbounds")]
		public ActionResult<PointListResponse> MotionBounds(int ClientID, int ClientTransactionID) {
			PointListResponse resp = new PointListResponse(ClientID, ClientTransactionID, "MotionBounds", null);
			try {
				resp.Value = Telescope.MotionBounds;
			} catch (Exception e) {
				resp.ErrorNumber = ASCOM.ErrorCodes.DriverBase+1;
				resp.ErrorMessage = e.Message;
			}
			return resp;
		}
		[HttpGet("register")]
		public ActionResult GetRegister([FromForm] int unit, string name) {
			return NotFound(string.Format("GetRegister({0}) is not imlpemented yet", name));
		}
		[HttpPut("register")]
		public ActionResult PutRegister([FromForm]int unit, [FromForm]string name, [FromForm]string value) {
			return NotFound(string.Format("PutRegister({0}, {1}) is not imlpemented yet", name, value));
		}
		[HttpGet("registers")]
		public ActionResult GetRegisters([FromForm] int unit, int start, int count) {
			return NotFound(string.Format("GetRegisters({0}, {1}) is not imlpemented yet", start, count));
		}
		[HttpGet("hourangle")]
		public ActionResult<DoubleResponse> GetHa(int ClientID, int ClientTransactionID) {
			double Ha = Telescope.GetPositionEarth().x * 24;
			return new DoubleResponse(ClientID, ClientTransactionID, "GetHa", Ha);
		}
		[HttpGet("positionearth")]
		public ActionResult<PointResponse> GetPositionEarth(int ClientID, int ClientTransactionID) {
			Sintez.Geometry.Point p = Telescope.GetPositionEarth();
			p.x *= 24;
			p.y *= 360;
			return new PointResponse(ClientID, ClientTransactionID, "GetPositionEarth", p);
		}
		[HttpPut("slewtohadecasync")]
		[Consumes("application/x-www-form-urlencoded")]
		public ActionResult<MethodResponse> SlewToHaDecAsync([FromForm] int ClientID, [FromForm] int ClientTransactionID, [FromForm] double Ha, [FromForm] double Dec, [FromForm] double Speed, [FromForm] bool fAlwaysMove) {
			MethodResponse response = new MethodResponse(ClientID, ClientTransactionID, "SlewToHaDecAsync");
			try {
				Sintez.Geometry.Point p = new Sintez.Geometry.Point(Ha/24, Dec/360);
				if (fAlwaysMove) {
					if (Math.Abs(Speed) > 1e-6) {
						Telescope.MoveBegin(p, true, true, Speed);
					} else {
						Telescope.MoveBegin(p, true, true);
					}
				} else {
					if (Math.Abs(Speed) > 1e-6) {
						Telescope.MoveBegin(p, true, false, Speed);
					} else {
						Telescope.MoveBegin(p, true);
					}
				}
			} catch (Exception e) {
				response.ErrorNumber = ASCOM.ErrorCodes.DriverBase+1;
				response.ErrorMessage = e.Message;
			}
			return response;
		}
	}
}
