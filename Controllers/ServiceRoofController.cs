﻿using System;
using System.Collections;
using System.Collections.Generic;
//using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ASCOMCore;
//using ASCOM.DeviceInterface;
using Sintez.Utils.Log;
using Sintez.IO;
using System.Web;

namespace Sintez
{
	public class RoofFullStatusResponse {
		public bool Connected { get; set; }
		public double LinkQuality { get; set; }
		public int Version { get; set; }
		public string VersionText { get; set; } = "";
		public int Status { get; set; }
		public string StatusText { get; set; } = "";
		public int ExtStatus { get; set; }
		public string ExtStatusText { get; set; } = "";
		public int Command { get; set; }
		public int ExtCommand { get; set; }
		public bool IsOpened { get; set; }
		public bool IsOpening { get; set; }
		public bool IsClosing { get; set; }
		public bool IsClosed { get; set; }
		public bool IsRain { get; set; }
		public bool IsError { get; set; }
		public int ErrorCode { get; set; }
		public bool IsRainDisabled { get; set; }
		public bool IsPowerOn { get; set; }
		public bool IsEqPowerOn { get; set; }
		public bool IsLightOn { get; set; }
		public bool IsTelescopeSafePosition { get; set; }
		public bool IsLimit { get; set; }
		public bool IsEmergencyOff { get; set; }
	}

	[ApiController]
	[ApiExplorerSettings(IgnoreApi = false)]
	[Route("/roof/")]
	public class ServiceRoofController : Controller
	{
		public static Sintez.IO.SintezRoof RoofController { get; set; }
		const int KeyUnlock = 0x4C86;
		[HttpGet("")]
		[HttpGet("index")]
		public ActionResult Index() {
			/*
			if (RoofController != null) {
				ViewBag.Connected = RoofController.IsConnected();
				ViewBag.RoofVersion = RoofController.getVersion();
				ViewBag.RoofStatus = RoofController.getStatus();
				ViewBag.IsOpened = RoofController.IsOpened;
				ViewBag.IsClosed = RoofController.IsClosed;
				ViewBag.IsOpening = RoofController.IsOpening;
				ViewBag.IsClosing = RoofController.IsClosing;
				ViewBag.IsError = RoofController.IsError;
				ViewBag.IsRain = RoofController.IsRain;
				ViewBag.IsRainDisabled = RoofController.IsRainDisabled;
				ViewBag.IsPowerOn = RoofController.Power;
				ViewBag.IsEqPowerOn = RoofController.EqPower;
				ViewBag.IsLight = RoofController.CamLight;
				ViewBag.IsSafePosition = RoofController.IsOpened;
			}
			*/
			return View("roof");
		}

		[HttpGet("service/status")]
		[Produces("application/json")]
		public ActionResult Status() {
			RoofFullStatusResponse roofStatus = new RoofFullStatusResponse();
			if (RoofController != null) {
				roofStatus.Connected = RoofController.IsConnected();
				roofStatus.LinkQuality = RoofController.LinkQuality;
				roofStatus.Version = RoofController.getVersion();
				roofStatus.VersionText = string.Format("{0}.{1}",
					(roofStatus.Version >> 8),
					(roofStatus.Version & 0xFF)
				);
				roofStatus.Status = RoofController.getStatus();
				roofStatus.Command = RoofController.getCommand();
				roofStatus.ExtCommand = RoofController.getExtCommand();
				roofStatus.IsOpened = RoofController.IsOpened;
				roofStatus.IsClosed = RoofController.IsClosed;
				roofStatus.IsOpening = RoofController.IsOpening;
				roofStatus.IsClosing = RoofController.IsClosing;
				roofStatus.IsError = RoofController.IsError;
				roofStatus.IsRain = RoofController.IsRain;
				roofStatus.IsRainDisabled = RoofController.IsRainDisabled;
				roofStatus.IsPowerOn = RoofController.Power;
				roofStatus.IsEqPowerOn = RoofController.EqPower;
				roofStatus.IsLightOn = RoofController.CamLight;
				roofStatus.IsTelescopeSafePosition = RoofController.IsOpened;
				roofStatus.IsLimit = RoofController.IsLimit;
				roofStatus.IsEmergencyOff = RoofController.IsEmergencyOff;
				roofStatus.ErrorCode = RoofController.getStatus() >> 8;

				string status1 = "";
				if (RoofController.IsClosed) status1 += "Закрыто";
				else if (RoofController.IsClosing) status1 += "Закрывается";
				else if (RoofController.IsOpened) status1 += "Открыто";
				else if (RoofController.IsOpening) status1 += "Открывается";
				else if (RoofController.IsError) status1 += string.Format("Ошибка {0}", roofStatus.ErrorCode);
				else status1 += "Неизвестно";
				roofStatus.StatusText = status1;
				string status2 = "";
				if (RoofController.Power) status2 += " Сила ВКЛ";
				if (RoofController.EqPower) status2 += " 12В ВКЛ";
				if (RoofController.IsRain) status2 += " ДОЖДЬ";
				if (RoofController.IsRainDisabled) status2 += " Датч. ОТКЛ";
				if (RoofController.IsLimit) status2 += " КОНЦЕВИК";
				if (RoofController.IsEmergencyOff) status2 += " ГРИБОК";
				roofStatus.ExtStatusText = status2;
			}
			return Ok(roofStatus);
		}
		[HttpPut("service/park")]
		[HttpPost("service/park")]
		public ActionResult Park() {
			RoofController.ModbusService.UnitRoof.sendParam(RoofUnit.reg_command_no, RoofUnit.cmd_park);
			return Ok();
		}

		[HttpPut("service/open")]
		public ActionResult RoofOpen() {
			RoofController.RoofOpen();
			return Ok();
		}
		[HttpPut("service/close")]
		public ActionResult RoofClose() {
			RoofController.RoofClose();
			return Ok();
		}
		[HttpPut("service/poweron")]
		public ActionResult PowerOn() {
			RoofController.Power = true;
			return Ok();
		}
		[HttpPut("service/poweroff")]
		public ActionResult PowerOff() {
			RoofController.Power = false;
			return Ok();
		}
		[HttpPut("service/powerunlock")]
		public ActionResult PowerUnlock() {
			RoofController.ModbusService.UnitRoof.sendParam(RoofUnit.reg_unlock, KeyUnlock);
			return Ok();
		}
		[HttpPut("service/eqpoweron")]
		public ActionResult EqPowerOn() {
			RoofController.EqPower = false;
			return Ok();
		}
		[HttpPut("service/eqpoweroff")]
		public ActionResult EqPowerOff() {
			RoofController.EqPower = false;
			return Ok();
		}
		[HttpPut("service/eqreset")]
		public ActionResult EqReset() {
			RoofController.ResetEq();
			return Ok();
		}
		[HttpPut("service/lighton")]
		public ActionResult LigthOn() {
			RoofController.setCamLight(true);
			return Ok();
		}
		[HttpPut("service/lightoff")]
		public ActionResult LigthOff() {
			RoofController.setCamLight(false);
			return Ok();
		}
		[HttpPut("service/raindisable")]
		public ActionResult RainDisable() {
			RoofController.SetRainDisable(true);
			return Ok();
		}
		[HttpPut("service/rainenable")]
		public ActionResult RainEnable() {
			RoofController.SetRainDisable(false);
			return Ok();
		}
	}
}
