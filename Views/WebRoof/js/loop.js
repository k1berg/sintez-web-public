// Обработчик веб-интерйеса крыши. Тут всё.
// Отсылка команд (по нажатию кнопок) через roofCommand и roofSerivceCommand, К счастью, они не имеют параметров
// С телескопом не так, но тут телескоп только читаем.

// Пути к endpointам
/*
const dome_api_url = '/api/v1/dome/0';
const dome_service_url = '/roof/service';
const telescope_service_url = '/service/telescope';
const management_url = '/management/v1';
const weather_info_url = '';
const telescope_api_url = '/api/v1/telescope/0';
*/
//const base_url = "http://192.168.88.11:5000";
//const base_url = "http://localhost:5000";
const base_url = "";
const dome_api_url = base_url+'/api/v1/dome/0';
const dome_service_url = base_url+'/roof/service';
const telescope_service_url = base_url+'/service/telescope';
const management_url = base_url+'/management/v1';
const weather_info_url = base_url+'';
const telescope_api_url = base_url+'/api/v1/telescope/0';

let status = { status: "unknown", errorNumber: -1000, errorMessage: "Ничего ещё не готово..." }; // GET /roof/service

// Счётчики успешно и неуспешно выполненных запросов
let serverSendCount = 0;
let networkErrorCount = 0;
let serverErrorCount = 0;
let serverSuccessCount = 0;

// Запросы, которые отправляются по таймеру (500мс)
const requests = [
	{ number: 0, path: management_url, command: "description", name:"desc", onload: onDescriptionReceived },
	{ number: 1, path: dome_api_url, command: "status", name: "roof", onload: onRoofStatusReceived },
	{ number: 2, path: dome_service_url, command: "status", name: "roof_ext", onload: onRoofExtStatusReceived },
	{ number: 3, path: telescope_service_url, command: "status", name: "telescope", onload: onTelescopeStatusReceived },
	{ number: 4, path: telescope_service_url, command: "motionbounds", onload: onMotionBoundsReceived },
	{ number: 5, path: telescope_service_url, command: "visiblebounds", onload: onVisibleBoundsReceived },
	{ number: 6, path: telescope_service_url, command: "register", params: { unit: 0, name: "parkpos" }, onload: onParkPosHaReceived },
	{ number: 7, path: telescope_service_url, command: "register", params: { unit: 1, name: "parkpos" }, onload: onParkPosDecReceived },
	{ number: 8, path: weather_info_url, command: "weather-info.json", name: "weather", onload: onWeatherReceived },
];

const request_sequence = [
	{ number: 0, timeout: 2000, delay: 10 },
	//{ number: 1, timeout: 500, delay: 300 },
	{ number: 2, timeout: 2000, delay: 10 },
	//{ number: 4, timeout: 2000, delay: 10 },
	{ number: 3, timeout: 2000, delay: 10 },
	//{ number: 1, timeout: 500, delay: 300 },
	//{ number: 5, timeout: 2000, delay: 10 },
	{ number: 2, timeout: 2000, delay: 10 },
	//{ number: 6, timeout: 2000, delay: 10 },
	{ number: 3, timeout: 2000, delay: 10 },
	//{ number: 1, timeout: 500, delay: 300 },
	//{ number: 7, timeout: 2000, delay: 10 },
	{ number: 2, timeout: 2000, delay: 10 },
	{ number: 8, timeout: 2000, delay: 10 },
	{ number: 3, timeout: 2000, delay: 10 },
];

const rs_idle = 0;
const rs_executing = 1;
const rs_delay = 4;
const rs_success = 2;
const rs_error = 3;

let current_request = {
	num: 0,
	time: -1,
	delay: -1,
	timeout: -1,
	state: rs_idle,
	name: "",
	result: {}
};
let loop_request = new AlpacaRequest(dome_service_url);

function onRequestTimer() {
	try {
	let tnow = Date.now();
	switch (current_request.state) {
		case rs_idle: { // Следующий запрос
			let n = current_request.num + 1;
			if (n<0) n = 0;
			if (n>=request_sequence.length) {
				n = 0;
				if (status.errorNumber == -1000) {
					status.errorNumber = 0;
					status.errorMessage = "Все данные приняты";
					try { document.getElementById("l_cmd_status").textContent = status.errorMessage; } catch(e) {}
				}
			}
			current_request.num = n;
			let req = requests[request_sequence[n].number];
			loop_request.baseUrl = req.path;
			loop_request.onload = null; //req.callback;
			loop_request.timeout = request_sequence[n].timeout;
			let s = null;
			current_request.result = null;
			current_request.onload = req.onload;
			current_request.onerror = req.onerror;
			current_request.name = req.name;
			current_request.timeout = request_sequence[n].timeout;
			try {
				if (req.params != null) {
					s = "";
					for(let i in req.params) {
						s = s + i+"="+req.params[i]+"&";
					}
				}
				current_request.time = tnow;
				current_request.delay = request_sequence[n].delay;
				current_request.timeout = request_sequence[n].timeout;
				
				loop_request.getValue(req.command, s);
				current_request.state = rs_executing;
			} catch (e) {
				current_request.state = rs_error;
				current_state.errorNumber = 9995;
				current_request.errorMessage = ""+e;
				logException(e, true);
			}
		} break;
		case rs_executing: {
			if (loop_request.fReady) {
				//if ((loop_request.response == null)
				//|| (loop_request.response.errorNumber != null && loop_request.response.errorNumber != 0)) {
				//	current_request.time = now;
				//	current_request.state = rs_error;
				//} else {
					current_request.time = tnow;
					current_request.state = rs_success;
				//}
			} else {
				if (checkTimeout(current_request.time, current_request.timeout)) {
					loop_request.abort();
					current_request.state = rs_error;
				}
			}
		} break;
		case rs_delay: { // Просто ждать
			if (checkTimeout(current_request.time, current_request.delay)) {
				current_request.time  = tnow;
				current_request.state = rs_idle;
			}
		} break;
		case rs_error: {
			if (loop_request != null && loop_request.request != null && loop_request.request.onloandend != null) loop_request.request.onloadend();
			current_request.result = incServerLinkCounters(loop_request);
			current_request.fReady = false;
			current_request.time  = tnow;
			current_request.state = rs_delay;
			if (current_request.onerror != null) {
				try { current_request.onerror(current_request.result); } catch(e) {logException(e);}
			}
		} break;
		case rs_success: {
			current_request.result = incServerLinkCounters(loop_request);
			if (current_request.name != null && current_request.result != null) {
				try {
					if (current_request.result.value != null) { // Если нормальная Alpaca прилетела - вытаскиваем value, а обёртка не нужна
						status[current_request.name] = current_request.result.value; 
					} else { // Если простой JSON без value - кладём целиком
						status[current_request.name] = current_request.result;
					}
				} catch(e) {logException(e);}
			}
			current_request.fReady = false;
			current_request.time  = tnow;
			current_request.state = rs_delay;
			if (current_request.onload != null) {
				try { current_request.onload(current_request.result); } catch(e) {logException(e);}
			}
		} break;
		default: {
			current_request.state = rs_delay;
			current_request.fReady = false;
			current_request.time  = tnow;
			current_request.num  = -1;

		} break;
	}
	if (serverSendCount > 200) {
		serverSendCount /= 4;
		networkErrorCount /= 4;
		serverErrorCount /= 4;
		serverSuccessCount /= 4;
	}
	updateServerStatus();
	} catch (e) { logException(e); }
}



/// <desc>Функция обновляет счётчики успешно и несупешно выполненных запросов к серверу</desc>
/// <param name="a">XMLHttpRequest, переданный в onload</param>
/// <comment> Вызывается по *request.onload. Если получено нормальный ответ (код 2хх), возвращает его
/// Если код 5хх, увеличивает счётчик ошибок. Если 4хх - нет, сам дурак.
/// Наконец если случился страшный EXCEPTION, то возвращает ответ { errorNumber: -1, errorMessage: e } и увеличивает счётчик ошибок
/// Таким образом, что бы не прилетело, вернётся всегда корректный ответ, похожий на ответ Alpaca
/// </comment>
function incServerLinkCounters(a) {
    try {
	serverSendCount++;
	if (a.response != null) {
	    if (a.response.errorNumber != null) {
		if ((a.response.errorNumber == -1) || (a.response.errorNumber > 2000 && a.response.errorNumber < 10000)) { // Ошибка сети
		    networkErrorCount++;
		} else if (a.response.errorNumber >= 500 && a.response.errorNumber < 599) { // Ошибка сервера
		    serverErrorCount++;
		} else serverSuccessCount++;
	    } else {
		if (a.responseCode >= 500 && a.responseCode <= 599) serverErrorCount++;
		else if (a.responseCode <= 0) networkErrorCount++;
		else if (a.responseCode >= 200 && a.responseCode <= 499) serverSuccessCount++
		else ;
	    }
            if (a.response.errorNumber != null && a.response.errorNumber != 0) {
		status.errorNumber = a.response.errorNumber;
		status.errorMessage = a.response.errorMessage;
                return null;
            } else {
                return a.response;
            }
	}
    } catch (e) {
	serverErrorCount++;
            status.errorNumber = -1;
            status.errorMessage = String(e);
	return null; //{ errorNumber: -1, errorMessage: e };
    }
    return null;
}

setInterval(onRequestTimer, 50);

