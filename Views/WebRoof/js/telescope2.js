/*if (!String.prototype.trim) {
  (function() {
    // Вырезаем BOM и неразрывный пробел
    String.prototype.trim = function() {
      return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
    };
  })();
}*/
var currentDate = null;
// Может быть получена через API в будущем
// Как и эфемериды
const observatoryData = {
	code: "095",
	name: "CRAO/Nauchniy",
	lat: 34.01388,
	lon: 44.72777,
	elev: 600,
	tz: 3 
};
const siderealDaySec = 86164.0905309;
const siderealSpeed = 86400*15/siderealDaySec; //15.041084;

function getCurrentDate() {
	if (currentDate == null) return new Date();
	else return currentDate;
}

function setCurrentDate(d) {
	currentDate = d;
}

function onTimeChange() {
	let d = null;
	let v = document.getElementById("time_time");
	if (v != null) v = v.value;
	if (v != null) {
		d = Date.now() + parseFloat(v)*3600*1000;
	}
	v = document.getElementById("time_date");
	if (v != null) v = v.value;
	if (v != null) {
		if (d == null) d = Date.now();
		d += parseFloat(v)*24*3600*1000;
	}
	
	setCurrentDate(new Date(d));
}

function trim(s) {
	return s.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
}

// Num в градусах (часах), fmt="hms" - выводить разделители 00h00m00.5s "deg"=00°00'00.0"   ":"=00:00:00.5
function toSexagesimalStr(num, fmt) {
    let sig = (num<0);
    if (sig) num = -num;

    let h = Math.trunc(num);
    let m = Math.trunc(num*60) % 60;
    let s = (num*60 - Math.trunc(num*60))*60;
    var o = "";
    if (sig) o = "-";
    let sh = h.toFixed(0);
    let sm = m.toFixed(0);
    let ss = s.toFixed(1)
    if (sh.length < 2) sh = "0"+sh;
    if (sm.length < 2) sm = "0"+sm;
    if (ss[1]==".") ss = "0"+ss;

    if (fmt=="hms") {
    o = o + sh + "h" + sm + "m" + ss+"s";
    } else if (fmt=="deg") {
    o = o + sh + "°" + sm + "'" + ss+'"';
    } else if (fmt==":") {
    o = o + sh + ":" + sm + ":" + ss;
    } else if (fmt != null && fmt != "") {
    o = o + sh + fmt + sm + fmt + ss;
    } else {
    o = o + sh + " " + sm + " " + ss;
    }
    
    return o;
}

// Возвращает число double градусов или часов. Если разобрать не удалось - NaN
function parseSexagesimalStr(str) {
	try {
		let r = [0, 0, 0];
		let result;
		let s = str.replace(/[\sdms°\'\"]+/g, " ");
		let ss = trim(s).split(" ");
		if (ss.length >= 1) {
			r[0] = parseFloat(trim(ss[0]));
		} 
		if (ss.length >= 2) {
			r[1] = parseFloat(trim(ss[1]));
		} 
		if (ss.length >= 3) {
			r[2] = parseFloat(trim(ss[2]));
		}
		result = Math.abs(r[0]) + Math.abs(r[1]/60.0) + Math.abs(r[2]/3600.0);
		if (r[0] < 0 || r[1] < 0 || r[2] < 0) result = -result;
		else if (Math.abs(r[0])<1e-6) { // Проверка на минус ноль
			if (trim(ss[0]).charAt(0)=="-") result = -result;
		}
		return result;
	} catch (e) {
		//console.log(e);
		return NaN;
	}
}

function createSVGFromPolygon(poly, color, fill) {
    let s = "";//'<?xml version="1.0" standalone="yes"?><svg width="1.0" height="1.0" viewBox="0 0 1200 400" xmlns="http://www.w3.org/2000/svg" version="1.1">';
    //s += '<rect x=-0.2 y=-0.15 width=0.4 height=0.380 stroke="red" stroke-width="3e-3" fill="yellow" />';
    s += '<polygon fill="'+fill+'" stroke="'+color+'" stroke-width="2px" vector-effect="non-scaling-stroke" fill-transparency="0.1" ';
    s += 'points="';
    try {
    for (let i=0; i<poly.length; i++) {
	var x = 0.0;
	var y = 0.0;
	try {
	    x = poly[i].ra; y = poly[i].dec;
	} catch (e) {
	    try {
		x = poly[i][0]; y = poly[i][1];
	    } catch (e) { 
		x=0; y=0;
	    }
	}
	s += x.toString()+","+y.toString()+" ";
    }
    } catch (e) {}
    s += '" />';
    return s;
}
// TODO: Переписать через local_sidereal()
function calculateMapOffset() {
	let timeMapA = Date.UTC(2021, 9-1, 20,  21, 0, 0);
	let timeMapS = Date.UTC(2022, 3-1, 21,  21, 0, 0);
	let timeOffset = (observatoryData.lat/15.0) - observatoryData.tz; // Сдвиг обсерватории от местного астрономического времени ??? UTC по астрономическому времени
	let time = getCurrentDate().getTime(); // Что будет в другом часовом поясе?

	let fMapS = false;
	let tdSky = (time - timeMapA) / 1000.0;
	tdSky /= siderealDaySec;
	//tdSky *= 24.0;
	tdSky -= Math.round(tdSky);
	if (tdSky > 0.5 || tdSky < -0.5) { // Нужна карта S
		fMapS = true;
		tdSky = (time - timeMapS) / 1000.0;
		tdSky /= siderealDaySec;
		tdSky -= Math.round(tdSky);
		tdSky += 0.5;
	}
	tdSky += timeOffset/24.0;
	let mapOffset = tdSky;// / 24.0;
// 0.25...-0.75
	//mapOffset = -mapOffset;
	while (mapOffset<-0.0) mapOffset += 1.0;
	while (mapOffset>1.0) mapOffset -= 1.0;
	return mapOffset;
}


function createSVGOffset(ra, dec) {
    let s = "translate(";
    try {
	s += ""+ra+", "+dec;
    } catch (e) {}
    s += ")";
    return s;
}

function createHorizon(alt) {
	let s = '<path d="';
	if (alt==null) alt=0;
	let had = aatohad(-0.0, alt, observatoryData.lat, 0);
	let fInv = false;
	if (had[0]>12.0) {
		had[0] -= 24.0;
		fInv = true;
	}
	//s = s + ' M'+((had[0])/24.0)+' '+had[1]/360.0;
	s = s + ' M'+((had[0])/24.0)+' -0.5';
	for (let az=-0.0; az<=365.0; az+=10.0) {
		had = aatohad(az, alt, observatoryData.lat, 0);
		ha = had[0]/24.0;
		dec = had[1]/360.0;
		if (ha>0.5) {
			//if (!fInv) {
			//	fInv = true;
			//	s = s + ' L'+ha+' -0.5 L'+(ha-1.0)+' -0.5'+' L'+(ha-1.0)+' '+dec;
			//}
			ha -= 1.0;
		} else {
			//if (fInv) {
			//	fInv = false;
			//	s = s + ' L'+(ha-1.0)+' -0.5 L'+(ha)+' -0.5'+' L'+(ha)+' '+dec;
			//}
		}
		//console.log('{ "az": '+az+', "ha": '+had[0]+', "dec": '+had[1]+' },');
		s = s + ' L'+ha+' '+dec;
	}
	s += ' L'+ha+' -0.5"></path>';
	//s += '"></path>';
	return s;
}

function createMoon(phase) {
	let area = 0.0;
	let s = '<g>';//'<g transform="scale(0.03, 0.03)">';
	s += '<ellipse cx="0.0" cy="0.0" rx="0.5" ry="0.5" fill="#333" stroke="none" stroke-width="0.01" />';
	s += '<path fill="#AAA" stroke="none" stroke-width="0.01" d="M 0,0.5 ';
	if (phase < 0.25) {
		s += ' A '+(0.5-phase*2)+' 0.5 180 1 1 0.0,-0.5';
		s += ' A 0.5 0.5 180 1 0 0.0,0.5" />';
		area = phase*2;
	} else if (phase < 0.50) {
		s += ' A '+(phase*2-0.5)+' 0.5 180 1 0 0.0,-0.5';
		s += ' A 0.5 0.5 180 1 0 0.0,0.5" />';
		area = phase*2;
	} else if (phase < 0.750) {
		s += ' A '+(phase*2-1.5)+' 0.5 180 1 1 0.0,-0.5';
		s += ' A 0.5 0.5 180 1 1 0.0,0.5" />';
		area = 2.0-phase*2;
	} else {
		s += ' A '+(phase*2-1.5)+' 0.5 180 1 0 0.0,-0.5';
		s += ' A 0.5 0.5 180 1 1 0.0,0.5" />';
		area = 2.0-phase*2;
	}
	s += '<text x="-0.5" y="0.2" fill="#FFF" font-family="Arial" font-size="0.5"><tspan>';
	s += (area*100).toFixed(0) + '%';
	s += '</tspan></text>';
	s += '</g>';
	return s;
}

function updateStarmap() {
    try {
	mapOffset = calculateMapOffset();
	setElemAttribute("starmap", "transform", createSVGOffset(mapOffset, 0.0));
        setElemProperty("horizon", "innerHTML", createHorizon());
        setElemProperty("horizon-20", "innerHTML", createHorizon(20.0));
	let now = getCurrentDate();
	let y = now.getUTCFullYear();
	let m = now.getUTCMonth()+1;
	let d = now.getUTCDate();
	let h = now.getUTCHours();
	let tz = observatoryData.tz; //now.getTimezoneOffset();
	let lat = observatoryData.lon;
	let lon = observatoryData.lat;
	let moon_pos = MoonPos(y,m,d,h);
	let sun_pos = SunPos(y,m,d,h,lat,lon);
	moon_pos[1] /= 360.0;
	moon_pos[0] = (moon_pos[0])/24.0;
	if (moon_pos[0] > 1.0) moon_pos[0] -= 1.0;
	if (moon_pos[0] < 0.0) moon_pos[0] += 1.0;
	sun_pos[1] /= 360.0;
	sun_pos[0] = (sun_pos[0])/24.0;
	if (sun_pos[0] > 1.0) sun_pos[0] -= 1.0;
	if (sun_pos[0] < 0.0) sun_pos[0] += 1.0;
	// поправки на то, что карта не свёрнута в трубку, а листом - с какой стороны листа рисовать Луну?
	if (Math.abs(moon_pos[0] - mapOffset) > Math.abs(moon_pos[0] - mapOffset + 1.0)) moon_pos[0] += 1.0;
	else if (Math.abs(moon_pos[0] - mapOffset) > Math.abs(moon_pos[0] - mapOffset - 1.0)) moon_pos[0] -= 1.0;
	if (Math.abs(sun_pos[0] - mapOffset) > Math.abs(sun_pos[0] - mapOffset + 1.0)) sun_pos[0] += 1.0;
	else if (Math.abs(sun_pos[0] - mapOffset) > Math.abs(sun_pos[0] - mapOffset - 1.0)) sun_pos[0] -= 1.0;
        setElemProperty("moon_pos", "innerHTML", createMoon(MoonPhase(y,m,d,h)));
        setElemAttribute("moon_pos", "transform", createSVGOffset(moon_pos[0], moon_pos[1])+' scale(-0.02, -0.02)');
        setElemAttribute("sun_pos", "transform", createSVGOffset(sun_pos[0], sun_pos[1]));
        setElemProperty("img_moon_inner", "innerHTML", createMoon(MoonPhase(y,m,d,h)));
    } catch (e) { logException(e) }
}

///<desc>Прочитанные статусы выводит в элементы страницы</desc>
///<comment>Вызывается по таймеру</comment>
///TODO: Обновлять элемент только если его состояние изменилось. Нужно хранить предыдущее.
function onTelescopeStatusError(st) {
    setStyleDisabled("block_telescope");
    setStyleDisabled("block_telescope_status");
    setElemAttribute("tel_indicator", "opacity", "0.3");
}
function onTelescopeStatusReceived(st) {
    if (st==null) return onTelescopeStatusError(st);
    if (st.errorNumber != null && st.errorNumber != 0) return onTelescopeStatusError(st);
    setStyleEnabled("block_telescope");
    setStyleEnabled("block_telescope_status");
    setElemAttribute("tel_indicator", "opacity", "1.0");
    try {
	document.getElementById("l_tel_state").textContent = st.versionText;
	if (st.linkQuality<=0.0001) {
		st.linkQuality = 0;
		st.connected = false;
	}
	if (!st.connected) {
	    document.getElementById("l_tel_state").textContent = "Нет связи";
	    setElemColors("l_tel_state", 0.0);
	    document.getElementById("l_tel_link").textContent = "Нет связи";
	    setElemColors("l_tel_link", 0.0);
	} else {
	    setElemColors("l_tel_state", null);
	    document.getElementById("l_tel_link").textContent = st.linkQuality.toFixed(3);
	    setElemColors("l_tel_link", st.linkQuality);
	}
    } catch (e) { }
    try {
	if (st.isParked) {
	    setElemColors("b_tel_park", "ok");
	    setElemColors("b_tel_unpark", null);
	    document.getElementById("l_tel_parked").textContent = "Телескоп запаркован";
	    if (st.isTracking) {
	        setElemColors("b_tel_clock", "error");
		document.getElementById("l_tel_tracking").textContent = "ВКЛ";
		setElemColors("l_tel_tracking", "error");
	    } else {
	        setElemColors("b_tel_clock", null);
		document.getElementById("l_tel_tracking").textContent = "ОТКЛ";
		setElemColors("l_tel_tracking", "");
	    }
	} else {
	    setElemColors("b_tel_park", null);
	    setElemColors("b_tel_unpark", "ok");
	    document.getElementById("l_tel_parked").textContent = "Распаркован";
	    if (st.isTracking) {
	        setElemColors("b_tel_clock", "ok");
		document.getElementById("l_tel_tracking").textContent = "ВКЛ";
		setElemColors("l_tel_tracking", "ok");
	    } else {
	        setElemColors("b_tel_clock", "error");
		document.getElementById("l_tel_tracking").textContent = "ОТКЛ";
		setElemColors("l_tel_tracking", "error");
	    }
	}
        if (st.isSlewing) {
	    setElemColors("b_tel_goto", "warning");
        } else {
            let da = (st.targetRa - st.Ra);
            let dd = (st.targetDec - st.Dec);
            let d = Math.sqrt(da*da + dd*dd);
            if (d < 1e-3) {
		    setElemColors("b_tel_goto", "ok");
            } else {
		    setElemColors("b_tel_goto", null);
            }
        }
    } catch (e) { }
    try {
	setElemProperty("l_tel_ra", "textContent", toSexagesimalStr(st.ra * 24, "hms"));
	setElemProperty("l_tel_dec", "textContent", toSexagesimalStr(st.dec * 360, "deg"));
	setElemProperty("l_tel_ha", "textContent", toSexagesimalStr(st.ha * 24, "hms"));
	setElemProperty("l_tel_dec_gnd", "textContent", toSexagesimalStr(st.dec * 360, "deg"));
    } catch (e) { logException(e) }
    try {
	setElemAttribute("tel_position", "transform", createSVGOffset(st.ha, st.dec));
	setElemAttribute("tel_target_position", "transform", createSVGOffset(st.targetHa, st.targetDec));
	document.getElementById("starmap").setAttribute("transform", createSVGOffset(calculateMapOffset(), 0.0));
    } catch (e) {logException(e)}
    try {
	if (st.telescopePath != null) {
            setElemProperty("tel_path", "innerHTML", createSVGFromPolygon(st.telescopePath, "#FFFF00", "none"));
	} else {
            setElemProperty("tel_path", "innerHTML", "" );
	}
    } catch (e) {logException(e)}
    try {
        if (st.motionBounds != null) {
            status.motion_bounds = st.motionBounds;
            document.getElementById("tel_motion_bounds").innerHTML = createSVGFromPolygon(st.motionBounds, "#C07F7F", "none");
        }
        if (st.visibleBounds != null) {
            status.visible_bounds = st.visibleBounds;
            document.getElementById("tel_visible_bounds").innerHTML = createSVGFromPolygon(st.visibleBounds, "#7FC0C0", "none");
        }
        if (st.parkingPosition != null) {
            if ((st.parkingPosition.ra != null && st.parkingPosition.ra != NaN) 
            && (st.parkingPosition.dec != null && st.parkingPosition.dec != NaN)) {
                status.park_ha = st.parkingPosition.ra;
                status.park_dec = st.parkingPosition.dec;
                document.getElementById("tel_park_position").setAttribute("transform", createSVGOffset(st.parkingPosition.ra, st.parkingPosition.dec));
            }
        }
    } catch (e) {logException(e)}
    try {
        if ((st.isTracking && !st.isSlewing && !st.isParked && st.timeToLimit >= 0)) {
		let ttl_str = toSexagesimalStr(st.timeToLimit/3600.0, "hms");
		setElemProperty("tel_limit_time", "innerHTML", ttl_str);
		setElemAttribute("tel_ttl_indicator", "display", "true");
		if (st.timeToLimit < 30.0*60) {
			if (st.timeToLimit < 10.0*60) {
				setElemAttribute("tel_limit_time", "fill", "#C00");
			} else {
				setElemAttribute("tel_limit_time", "fill", "#FF0");
			}
		} else {
			setElemAttribute("tel_limit_time", "fill", "#FFF");
		}
	} else {
		setElemProperty("tel_limit_time", "innerHTML", "???");
		setElemAttribute("tel_ttl_indicator", "display", "none");
		setElemAttribute("tel_limit_time", "fill", "#FFF");
	}
    } catch (e) { logException(e); }
}

function onMotionBoundsReceived(mb) {
	if (mb != null && mb.value != null) {
		status.motion_bounds = mb.value; // global status
		document.getElementById("tel_motion_bounds").innerHTML = createSVGFromPolygon(mb.value, "C07F7F", "none");
	}
}
function onVisibleBoundsReceived(vb) {
	if (vb != null && vb.value != null) {
		status.visible_bounds = vb.value; // global status
		document.getElementById("tel_visible_bounds").innerHTML = createSVGFromPolygon(vb.value, "#7FC0C0", "none");
	}
	document.getElementById("starmap").setAttribute("transform", createSVGOffset(calculateMapOffset(), 0.0));
}

function onParkPosHaReceived(p) {
	if (p != null && p.value != null && p.value.value != null) {
		status.park_ha = p.value.value;
	}
}
function onParkPosDecReceived(p) {
	if (p != null && p.value != null && p.value.value != null) {
		status.park_dec = p.value.value;
		if ((status.park_ha != null && status.park_ha != NaN) 
		&& (status.park_dec != null && status.park_dec != NaN)) {
			document.getElementById("tel_park_position").setAttribute("transform", createSVGOffset(status.park_ha, status.park_dec));
		}
	}
}


// Послать команду контроллеру крыши (см. хелп АСКОМА)
function telescopeCommand(s, params) {
    let command_executor = new AlpacaRequest(telescope_api_url);
    command_executor.logRequests = true;
    command_executor.onload = function(a) {
	incServerLinkCounters(a);
    }
	let ps;
	if (params != null) {
		ps = "";
		for(let i in params) {
			ps = ps + i+"="+params[i]+"&";
		}
	}
    command_executor.command(s, ps);
    //serverSendCount++;
    return false;
}

function telescopeGoTo() {
	let ra_str = document.getElementById("i_tel_ra").value;
	let dec_str = document.getElementById("i_tel_dec").value;
	let ra = parseSexagesimalStr(ra_str);
	let dec = parseSexagesimalStr(dec_str);
	telescopeCommand("slewToCoordinatesAsync", { rightAscension: ra, declination: dec });
}

function telescopePark() {
	telescopeCommand("park");
}
function telescopeUnPark() {
	telescopeCommand("unpark");
}
function telescopeTracking() {
	let ts_str = document.getElementById("i_tel_clock").value;
	let ts = parseFloat(ts_str);
	let tc = status.telescope.speedRa * siderealDaySec; 
	let ds = ts - tc; // В единицах звёздной скорости. Тут можно просто взять GuideRateRightAscension из Alpaca
	let gr = siderealSpeed; // Звёздная скорость для альпаки
	if (ts > 0.01 && ts < 10 && !IsNaN(ds)) { // Введено какое-то разумное число
		gr = gr * (ts - 1.0);
	} else { // Неразумное число - полагаем что нужна звёздная скорость
		ds = 1.0 - tc;
                gr = 0.0;
	}
	if (status.telescope.isTracking) {
		if (Math.abs(ds) > 1e-3) { // Задана другая скорость
			telescopeCommand("guideRateRightAscension", {guideRateRightAscension: gr});
		} else { // Скорость та же, что у телескопа - остановка часового
			telescopeCommand("tracking", {tracking: false});
		}
	} else { // Ведение выключено
		telescopeCommand("tracking", {tracking: true});
		if (Math.abs(gr) > 1e-6) telescopeCommand("guideRateRightAscension", {guideRateRightAscension: gr});
	}
}

let telescope_direction = null;
let telescope_speed = 0;
let telescope_timer = 0;
let minJoystickSpeed = 30.0/3600.0; // Градусов в секунду, по альпачьи
let maxJoystickSpeed = 0.75;
let telescope_joy_timer = null;

function telescopeMoveClick(dir) {
    console.log("MoveClick("+dir+")");
	if (telescope_direction != null || dir == null) {
		telescopeStop();
	}
	telescope_direction = dir;
	telescopeMoveAxis(dir, minJoystickSpeed);
	telescope_joy_timer = setInterval(telescopeMoveTimer, 1000);
	document.body.onmouseup = telescopeMoveRelease;
}

function telescopeMoveAxis(dir, speed) {
    console.log("MoveAxis("+dir+", "+speed+")");
	let speedA = 0;
	let speedD = 0;
	switch(dir) {
		case "N":
			speedA = 0;
			speedD = speed;
		break;
		case "S":
			speedA = 0;
			speedD = -speed;
		break;
		case "E":
			speedA = -speed;
			speedD = 0;
		break;
		case "W":
			speedA = speed;
			speedD = 0;
		break;
		default:
			telescopeStop()
			telescope_direction = null;
	}
	if (speedA != 0) {
		telescope_speed = Math.abs(speedA);
		telescopeCommand("moveAxis", {axis: 0, rate: speedA});
	}
	if (speedD != 0) {
		telescope_speed = Math.abs(speedD);
		telescopeCommand("moveAxis", {axis: 1, rate: speedD});
	}
}

function telescopeStop() {
    console.log("telescopeStop()");
	telescopeCommand("moveAxis", {axis: 0, rate: 0});
	telescopeCommand("abortSlew");
	telescope_speed = 0;
	telescope_timer = 0;
	telescope_direction = null;
}

function telescopeMoveRelease() {
    console.log("MoveRelease()");
	if (telescope_direction != null) telescopeStop();
	telescope_direction = null;
	telescope_speed = 0;
	telescope_timer = 0;
    if (telescope_joy_timer) {
        clearInterval(telescope_joy_timer);
        telescope_joy_timer = null;
    }
}

function telescopeMoveTimer() {
    console.log("MoveTimer(), tel_dir="+telescope_direction+", tel_speed="+telescope_speed+", tel_timer="+telescope_timer);
	if (telescope_speed < -1e-6) telescope_speed = -telescope_speed;
	if (telescope_direction != null && telescope_speed > 1e-6) {
		if (telescope_speed < maxJoystickSpeed) {
			telescope_timer++;
			if (telescope_speed < 7.0/60.0) {
				telescope_speed *= 2.0;
			} else {
				telescope_speed *= 1.2;
			}
			telescopeMoveAxis(telescope_direction, telescope_speed);
		}
	} else {
		telescope_timer = 0;
	}
}

function coordsScreenToEarth(x, y) {

    let svg = document.getElementById("tel_indicator");
    let skymap = document.getElementById("starmap");
    let tel_indicator_hadec = document.getElementById("tel_indicator_hadec");
    let pt = svg.createSVGPoint();  // Created once for document
    let pt0 = svg.createSVGPoint();  // Created once for document
    let pt1 = svg.createSVGPoint();  // Created once for document
    pt.x = x;
    pt.y = y;
    let ha_ctm = tel_indicator_hadec.getCTM();
    let svg_ctm = svg.getScreenCTM();
    let pt_ha =  pt.matrixTransform(ha_ctm.inverse());
    pt0.x = 0; pt0.y = 0;
    let pt_center_svg = pt0.matrixTransform(svg_ctm);
    pt1.x = pt.x - pt_center_svg.x; // Вот зачем это ?!
    pt1.y = pt.y - pt_center_svg.y; // Вот зачем это ?!
    let pt2 = pt1.matrixTransform(ha_ctm.inverse());
    return pt2;
}
function coordsScreenToSky(x, y) {
    let p = coordsScreenToEarth(x, y);
    p.x = calculateMapOffset() - p.x;
    return p;
}
function telescopeIndicatorClick(ev) {
    try {
	console.log(ev);
	let x = ev.clientX;
	let y = ev.clientY;
	let pt_earth = coordsScreenToEarth(x, y);
	let pt_sky = coordsScreenToSky(x, y);
	if (pt_sky.x < 0) pt_sky.x += 1.0;
        let tx = pt_sky.x * 24;
        let ty = pt_sky.y * 360;
	setElemProperty("i_tel_ra", "value", toSexagesimalStr(tx));
	setElemProperty("i_tel_dec", "value", toSexagesimalStr(ty));
	setElemAttribute("tel_cursor_position", "transform", createSVGOffset(pt_earth.x, pt_earth.y));
    } catch(e) {
	logException(e);
    }
}

// Движение карты звёздного неба раз в минуту
updateStarmap();
setTimeout(60000, updateStarmap);
