// Имя картинки с камеры
let cameraImage = "01.jpg";
const cameras = [
	{ number: 0, path: "01-2.jpg", name: "КАМ1", label: "l_cam1" },
	{ number: 1, path: "02-2.jpg", name: "КАМ2", label: "l_cam2" },
	{ number: 2, path: "103-2.jpg", name: "Восток", label: "l_cam_sky" },
	{ number: 3, path: "allsky.jpg", name: "ВсёНебо", label: "l_cam_allsky" }
];


// Обработчк 5сек таймера - тут картинку перерисовываем
function cameraUpdate() {
	document.getElementById("img01").src = cameraImage+"?"+new Date().getTime();
	//document.getElementById("img103").src = "103.jpg?"+new Date().getTime();
	//document.getElementById("img02").src = "02.jpg?"+new Date().getTime();
	//document.getElementById("bgnd").style.backgroundImage = "url('01.jpg?" + new Date().getTime() + "')";
	//document.getElementById("bgnd").style.backgroundImage = "url('01.jpg')";
	//document.getElementByTagName("body").style.backgroundImage = "url('02.jpg?"+new Date().getTime()+"')";
}

function cameraChange(num) {
	try {
		cameraImage = cameras[num].path;
		for(let c in cameras) {
			l = document.getElementById(cameras[c].label);
			if (l != null) {
				l.textContent = cameras[c].name;
				if (cameras[c].number == num) {
					l.classList.add("active");
				} else {
					l.classList.remove("active");
				}
			}
		}
		//getDocumentById("l_cam_name").textContent = cameras[num].name;
		cameraUpdate();
	} catch (e) { logException(e); }
}


setInterval(cameraUpdate, 5000);
