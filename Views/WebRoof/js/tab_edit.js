
function updateConfigTable(id, dataFunc) {
	let tab = document.getElementById(id);
	if (tab==null) return;

	let len = dataFunc();
	if (len==null || len<=0) return;
	let tablen = tab.rows.length;
	
	for (i=0; i<len; i++) {
		try {
			/*const name = telescope_regs.config[i].name;
			const val = telescope_regs.config[i].value;
			const str = telescope_regs.config[i].asString;
			const dbl = telescope_regs.config[i].asDouble;*/
			const data = dataFunc(i);
			const name = data[0].value;
			if (i==0) {
				if ("true"==tab.rows[1].getAttribute("data-delete")) {
					tab.rows[1].cells[0].innerText = name;
				}
			}

			let j = i;
			let found = false;
			do {
				if (i>=tablen) break;
				if (j >= tablen) j = 0;
				let row = tab.rows[j];
				let cell_name = row.cells[0].innerText;
				if (cell_name.toUpperCase() == name.toUpperCase()) {
					for (let x=1; x<data.length; x++) {
						d = data[x];
						if (d != null) {
							cell = row.cells[x];
							if (!cell) cell = row.insertCell();
							if ( ("true" != row.cells[x].getAttribute("changed"))
							&& ("true" != row.cells[x].getAttribute("editing")) ) {
								row.cells[x].innerText = d.value;
								row.cells[x].setAttribute("editable", d.editable);
							}
						}
					}
					found = true;
					break;
				}
				j = j + 1;
			} while (j != i);
			if (!found) {
				let new_row = tab.insertRow();
				let new_cell = new_row.insertCell();
				new_cell.innerText = name;
				for (let x=1; x<data.length; x++) {
					d = data[x];
					new_cell = new_row.insertCell();
					if (d != null) {
						new_cell.innerText = d.value;
						new_cell.setAttribute("editable", d.editable);
					} else {
						new_cell.innerText = "null";
						new_cell.setAttribute("editable", false);
					}
				}
			}
		} catch (e) {
			console.log(e);
		}
	}	
}

//let fCellEditing = false;
let cell_col = -1;
let cell_row = -1;
let cell_prev_text = null;
let changed_count = 0;

let paramList = null;
let paramIndex = -1;

function tableEditApply(edit_cell) {
	if (!edit_cell) return;
	const cell_input = document.getElementById('cell_edit');
	if (!cell_input) return;
	const t_new = cell_input.value;
	if (cell_prev_text != t_new) { // Текст изменился?
		edit_cell.innerText = t_new; // cellInput должен удалиться
		edit_cell.setAttribute("changed", "true");
		edit_cell.removeAttribute("editing");
		setBackground(edit_cell, "#FFC0C0");
		changed_count++;
		setElemStyle("buttons", "display", "block");
		setElemProperty("t_changed", "innerText", changed_count+" регистров для записи");
	} else { // Не изменился - отмена
		tableEditCancel(edit_cell);
	}
	cell_prev_text = null;
	cell_col = -1;
	cell_row = -1;
}

function tableEditCancel(edit_cell) {
	if (!edit_cell) return;
	if ("true"==edit_cell.getAttribute("changed")) { // Уже было изменено раньше
		setBackground(edit_cell, "#FFC0C0");
	} else {
		setBackground(edit_cell, "#C0C0FF");
	}
	edit_cell.innerText = cell_prev_text; // cellInput должен удалиться
	edit_cell.removeAttribute("editing");
}

function getContentWidth (element) {
	let styles = getComputedStyle(element)
	return (element.clientWidth
		- parseFloat(styles.paddingLeft)
		- parseFloat(styles.paddingRight));
}

function tableOnClick(ev, tab_name) {
	const cellInput = document.getElementById('cell_edit');
	const cell = ev.target.closest('td');
	if (cellInput) {
		if (ev.currentTarget==cellInput) return; // Клик в строке ввода - не обрабатываем
		const tab = document.getElementById(tab_name);
		const edit_cell = tab.rows[cell_row].cells[cell_col]; // Текущая редактируемая ячейка
		if (cell != edit_cell) { // Клик по другой = Apply
			tableEditApply(edit_cell);
		}
	} else {
		if (!cell) return; // Клик не в таблицу
		const row = cell.parentElement;
		console.log(cell.innerHTML, row.rowIndex, cell.cellIndex);
		if ("true" == cell.getAttribute("editable")) {
			cell.setAttribute("editing", "true"); // Чтобы пришедший json не затёр
			//fCellEditing = true;
			setBackground(cell, "#FFFFE0");
			const s = cell.innerText;
			cell_prev_text = s;
			//cell.innerHTML = '<input id="cell_'+row.rowIndex+'_'+cell.cellIndex+'" value="'+s+'"></input>'
			cell.innerHTML = '<input id="cell_edit" onkeydown="inputKeyPress(event, '+"'"+tab_name+"'"+')" value="'+s+'"></input>'
			cell_col = cell.cellIndex;
			cell_row = row.rowIndex;
			let inp = document.getElementById("cell_edit");
			if (inp) {
				inp.focus();
				inp.setAttribute("value", s);
				setElemStyle(inp, "width", getContentWidth(cell)+"px");
			}
		}
	}
}

function inputKeyPress(ev, tab_name) {
	switch(ev.key) {
		case "Enter":
			if (cell_row>0 && cell_col>0) {
				const tab = document.getElementById(tab_name);
				const edit_cell = tab.rows[cell_row].cells[cell_col]; // Текущая редактируемая ячейка
				tableEditApply(edit_cell);
			}
		break;
		case "Escape":
			if (cell_row>0 && cell_col>0) {
				const tab = document.getElementById(tab_name);
				const edit_cell = tab.rows[cell_row].cells[cell_col]; // Текущая редактируемая ячейка
				tableEditCancel(edit_cell);
			}
		break;
	}
}


function saveChanges(tab_name) {
	const tab = document.getElementById(tab_name);
	if (!tab) return;
	const len = tab.rows.length;
	//let tab_name = tab.getAttribute("id");
	paramList = [];
	for (let i=0; i<len; i++) {
		let row = tab.rows[i];
		let rowlen = row.cells.length;
		const name = row.cells[0].innerText; //cell.getAttribute("name");
		for (j=1; j<=rowlen; j++) {
			let cell = row.cells[j];
			if (cell) {
				if (("true"==cell.getAttribute("editable"))
				&& ("true"==cell.getAttribute("changed")) ) {
					const addr = parseInt(row.cells[1].innerText); //cell.getAttribute("addr");
					const value = cell.innerText;
					let param = { type: 1, table: tab_name, row: i, cell: j, name, addr, value };
					paramList.push(param);
					//cell.removeAttribute("changed");
					//setBackground(cell, null);
					//changed_count--;
				}
			}
		}
	}
	console.log(paramList);
	if (paramList.length > 0) {
		paramIndex = 0;
	} else {
		paramList = null;
	}
	//if (changed_count != 0) console.log("changed_count!!!! "+changed_count);
	//else setElemStyle("buttons", "display", "none");
}

function cancelChanges(tab_name) {
	//changed_count = 0;
	const tab = document.getElementById(tab_name);
	if (!tab) return;
	const len = tab.rows.length;
	//let tab_name = tab.getAttribute("id");
	for (let i=0; i<len; i++) {
		let row = tab.rows[i];
		let rowlen = row.cells.length;
		const name = row.cells[0].innerText; //cell.getAttribute("name");
		for (j=1; j<=rowlen; j++) {
			let cell = row.cells[j];
			if (cell) {
				if (("true"==cell.getAttribute("editable"))
				&& ("true"==cell.getAttribute("changed")) ) {
					cell.removeAttribute("changed");
					setBackground(cell, null);
					changed_count--;
				}
			}
		}
	}

	if (changed_count != 0) console.log("changed_count!!!! "+changed_count);
	else setElemStyle("buttons", "display", "none");
	paramList = [];
}
