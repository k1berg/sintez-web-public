function logException(e, fSetStatus) {
	try {
		let s = ":"+e.lineNumber+":"+e.columnNumber+"  "+e.message;
		let f = e.fileName;
		try {
			let i = lastIndexOf(f, '/');
			if (i>0) f = slice(i+1, f.length);
		} catch(ee) {}
		s = f + s;
		console.log(s);
		if (fSetStatus==true) {
			status.errorNumber = 9994;
			status.errorMessage = s;
		}
	} catch (ee) {}
}


// Установить цвет объекту по имени
function setBackground(obj, bg) {
    let f;
    try {
    if (typeof obj == "string") {
        f = document.getElementById(obj);
    } else {
        f = obj;
    }
    if (f == null) return;
    if (bg != null && bg != "") {
        f.style.backgroundColor = bg;
    } else {
        f.style.backgroundColor = null;
    }
    } catch (e) { logException(e); }
}

// Установить стиль по имени
function setElemStyle(obj, style, value) {
    let f;
    try {
    if (typeof obj == "string") {
        f = document.getElementById(obj);
    } else {
        f = obj;
    }
    if (f == null) return;
    if (value != null && value != "") {
        f.style[style] = value;
    } else {
        f.style[style] = null;
    }
    } catch (e) { logException(e); }
}

// Установить произвольное свойство по имени
function setElemProperty(obj, prop, value) {
    let f;
    try {
    if (typeof obj == "string") {
        f = document.getElementById(obj);
    } else {
        f = obj;
    }
    if (f == null) return;
    if (value != null && value != "") {
        f[prop] = value;
    } else {
        f[prop] = null;
    }
    } catch (e) { logException(e); }
}

// Установить атрибут значение атрибута по имени
function setElemAttribute(obj, prop, value) {
    let f;
    try {
    if (typeof obj == "string") {
        f = document.getElementById(obj);
    } else {
        f = obj;
    }
    if (f == null) return;
    if (value != null && value != "") {
        f.setAttribute(prop, value);
    } else {
        f.removeAttribute(prop);
    }
    } catch (e) { logException(e); }
}

// Раскрасить объект/блок нормальным цветом. Если fEna==false, красит серым как setStyleDisabled
function setStyleEnabled(obj, fEna) {
	if (fEna==null) fEna=true;
	try {
		if (typeof obj == "string") f = document.getElementById(obj);
		else f = obj;
		if (f == null) return;
		if (!fEna) f.classList.add("disabled");
		else f.classList.remove("disabled");
	} catch (e) { logException(e); }
	/*setElemStyle(obj, "color", (fEna ? null : "#808080"));
	setElemStyle(obj, "backgroundColor", (fEna ? null : "#C0C0C0"));*/
}

// Раскрасить объект/блок серым цветом - признак неработоспособности/некорректных данных
function setStyleDisabled(obj) {
	setStyleEnabled(obj, false);
	/*setElemStyle(obj, "color", "#808080");
	setElemStyle(obj, "color", "#C0C0C0");*/
}

function linkColor(q) {
    c = "#FF0000";
    if (q > 0.99) c = "#40FF40"; // Color.LightGreen;
    else if (q > 0.97) c = "#C0FF40"; // Color.GreenYellow;
    else if (q > 0.90) c = "#FFFF40"; // Color.Yellow;
    else if (q > 0.80) c = "#FFC040"; // Color.Orange;
    else if (q > 0.7) c = "#FF8040"; // Color.OrangeRed;
    else if (q > 0.5) c = "#FF4000"; // Color.OrangeRed;
    else if (q > 0.3) c = "#FF0000"; // Color.Red;
    else c = "#FF00FF"; // Color.Pink;
    return c;
}

function isDarkTheme() {
    try {
        d = document.getElementById("background");
        if (!d.classList.contains("color-light")) return true;
    } catch (e) { logException(e); }
    return false;
}

/// Красит элемент в зависимости от текущей темы
/// Type:
///  2, "ERROR" - Красный (нештатное состояние)
///  3, "WARNING" - Желтый (внимание или переходный процесс)
///  4, "OK" - Зелёный (рабочее состояние)
///  null - цвет по умолчанию
/// 0.0 - 1.0 - цвет качества связи
function setElemColors(obj, type) {
    let clr_Error = '#FF7F7F';
    let clr_Warning = '#FFFF7F';
    let clr_Ok = '#7FFF7F';
    if (isDarkTheme()) {
        clr_Ok = "#1FBF1F";
        clr_Warning= "#BFBF1F";
    }
    try {
        let f;
        let c;
        if (typeof obj == "string") {
            f = document.getElementById(obj);
        } else {
            f = obj;
        }
        if (f == null) return;
        if (typeof type == "string") type = type.toUpperCase();
        if (typeof type =="number" && (type>=0.0 && type<=1.0)) { // Качество связи
            c = linkColor(type);
        } else switch(type) {
            case "ERROR":
            case 2:
                c = clr_Error;
            break;
            case "WARNING":
            case 3:
                c = clr_Warning;
            break;
            case "OK":
            case 4:
                c = clr_Ok;
            break;
            default:
                c = null;
        }
        if (isDarkTheme()) {
            f.style.backgroundColor = null;
            f.style.color = c;
        } else {
            f.style.backgroundColor = c;
            f.style.color = null;
        }
    } catch (e) { logException(e); }
}

// Запретить отсылать POST при нажатии любой кнопки на форме
function disableFormsPost() {
    try {
        let forms = document.getElementsByTagName("form");
        for (let f=0; f<forms.length; f++) {
	    try {
        	    forms[f].addEventListener('submit', function(e) {
        	        e.preventDefault();
        	    });
	    } catch (e) { logException(e); }
        }
    } catch (e) { logException(e); }
}


/// @desc Возвращает true, если текущее время > begin_time+timeout
/// если аргументы null, undefined или неположительны, тоже возвращает true
/// и при любой ошибке возвращает true
/// @desc false только если с момента begin_time ещё не прошло timeout миллисекунд времени
function checkTimeout(begin_time, timeout) {
	try {
		if (begin_time==null || begin_time <= 0) return true;
		if (timeout==null || timeout <= 0) return true;
		let tnow = Date.now();
		if (tnow > begin_time+timeout) return true;
		else return false;
	} catch (e) {
		return true;
	}
}

