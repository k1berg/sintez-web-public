/***
Based on articles
http://alienryderflex.com/polygon/
 Point-In-Polygon Algorithm � Determining Whether A Point Is Inside A Complex Polygon
http://alienryderflex.com/shortest_path/
 Shortest Path Through A Concave Polygon With Holes
� 1998,2006,2007 Darel Rex Finley.
Public-domain code by Darel Rex Finley, 2006.

Some errors fixed
Rewritten in C#
Simplified for testing only one polygon, not multiple.
If you wish, you may add List<Polygon> inside main poly and test around all, like in original code

Alexey Harchenko, 2020

***/

using System.Collections.Generic;
using System;
using System.Text.Json.Serialization;

namespace Sintez.Geometry
{
	public enum PointLinePos {
		Left, // �����, ���� �������� �� ������ � ����� ������� 
		Right, // ������ 
		Before, // ����� �����
		Behind, // ����� �������
		Between, // �� �������
		Start, // ��������� � ������� +-EPS
		End, // ��������� � ������ +-EPS
		Contains, // ��������� � ������������ ������ +-EPS
		Outside // �� ��������� � ������������ ������
	}
	public class Point
	{
		public static double EPS = 1e-6;
		[JsonIgnoreAttribute]
		public double x { get; set; }
		[JsonIgnoreAttribute]
		public double y { get; set; }
		public double Ra {  get { return x; } set { x = value; } }
		public double Dec { get { return x; } set { x = value; } }
		public Point(Point p) { x = p.x; y = p.y; }
		public Point(double nx, double ny) { x = nx; y = ny; }
		public Point(double[] pos) { x = pos[0]; y = pos[1]; } // can throw null pointer reference
		public static Point operator -(Point p, Point q) { return new Point(p.x - q.x, p.y - q.y); }
		public static Point operator +(Point p, Point q) { return new Point(p.x + q.x, p.y + q.y); }
		public static Point operator *(double scale, Point p) { return new Point(p.x * scale, p.y * scale); }
		public static Point operator *(Point p, double scale) { return new Point(p.x * scale, p.y * scale); }
		public static Point operator /(Point p, double scale) { return new Point(p.x / scale, p.y / scale); }

		[JsonIgnoreAttribute]
		public double r { get { return Math.Sqrt(x * x + y * y); } }
		public double Distance(Point p) {
			if (p == null) return Math.Sqrt(x * x + y * y);
			else {
				double dx = x - p.x, dy = y - p.y;
				return Math.Sqrt(dx * dx + dy * dy);
			}
		}
		// ������������� �� ����� p0 �� ������� (p1, p2)
		// ���������� �� ������.
		// ���������� null ���� �������������� �� ��������� ������ ��� ������ ����� ��������, ��� ������� null ����� ����������
		// ���� �� ������, ���������� ���������� �������� ����� �� ������� (���� ���� �������������)
		public static Point Perpendicular(Point p0, Point p1, Point p2) {
			if (p0==null || p1==null || p2==null) return null;
			Point d = p2 - p1;
			double ds = (d.x*d.x + d.y*d.y);
			//double t = (d.x*p0.x + d.y*p0.y) - (d.x*p1.x + d.y*p1.y);
			double t = d.x * (p1.x - p0.x) + d.y * (p1.y - p0.y);
			if (Math.Abs(ds) < Math.Abs(t)*EPS) return null;
			t /= -ds;
			if (t<-EPS || t>(1.0+EPS)) return null;
			double xx = d.x * t + p1.x;
			double yy = d.y * t + p1.y;
			return new Point(xx, yy);
		}
		public Point Perpendicular(Point p1, Point p2) {
			return Perpendicular(this, p1, p2);
		}

		/*
		// ����������� ���� ��������. ������� �� � �����, �� � �������� ��������� ���� �� �����
		// ��������� ������������ [pipj x pipk]. �� ������������ �� ����� �����
		public static double VectorProd(Point pi, Point pj, Point pk) { // pi - ����������� �����
			double rc = (pk.x - pi.x) * (pj.y - pi.y) - (pj.x - pi.x) * (pk.y - pi.y);
			return rc;
		}
		// ���������� 1 ���� ����� pk+-EPS ����������� ������� [pi, pj], -1 ���� �� �����������, � 0 ���� �� �������
		protected static int OnSegment(Point pi, Point pj, Point pk) {
			if ((Math.Min(pi.x, pj.x) < pk.x - EPS)
			&& (Math.Max(pi.x, pj.x) > pk.x + EPS)
			&& (Math.Min(pi.y, pj.y) < pk.y - EPS)
			&& (Math.Max(pi.y, pj.y) > pk.y + EPS)) {
				return 1;
			} else if ((Math.Min(pi.x, pj.x) < pk.x + EPS)
			&& (Math.Max(pi.x, pj.x) > pk.x - EPS)
			&& (Math.Min(pi.y, pj.y) < pk.y + EPS)
			&& (Math.Max(pi.y, pj.y) > pk.y - EPS)) {
				return 0;
			} else return -1;
		}
		// <0 - �� ���������� >0 - ���������� ==0 - �� ������� (+-EPS)
		protected static int TestIntersection0(Point a1, Point a2, Point b1, Point b2) {
			double d1 = VectorProd(b1, b2, a1);
			double d2 = VectorProd(b1, b2, a2);
			double d3 = VectorProd(a1, a2, b1);
			double d4 = VectorProd(a1, a2, b2);

			if (((d1 >= EPS && d2 <= EPS) || (d1 <= EPS && d2 >= EPS))
				&& ((d3 >= EPS && d4 <= EPS) || (d3 <= EPS && d4 >= EPS))) {
				return 1; // ����� ����������
			} else {
				if ((Math.Abs(d1) <= EPS) && OnSegment(b1, b2, a1)>=0) return 0;
				if ((Math.Abs(d2) <= EPS) && OnSegment(b1, b2, a2)>=0) return 0;
				if ((Math.Abs(d3) <= EPS) && OnSegment(a1, a2, b1)>=0) return 0;
				if ((Math.Abs(d4) <= EPS) && OnSegment(a1, a2, b2)>=0) return 0;
			}
			return -1; // ����� �� ����������
		}
		public static int TestIntersection(Point a1, Point a2, Point b1, Point b2) {
			PointLinePos fa1 = a1.PointLinePosition(b1, b2);
			PointLinePos fa2 = a2.PointLinePosition(b1, b2);
			PointLinePos fb1 = b1.PointLinePosition(a1, a2);
			PointLinePos fb2 = b2.PointLinePosition(a1, a2);
			if (fa1 == PointLinePos.Between || fa1 == PointLinePos.Start || fa1 == PointLinePos.End) return 0;
			if (fa2 == PointLinePos.Between || fa2 == PointLinePos.Start || fa2 == PointLinePos.End) return 0;
			if (fb1 == PointLinePos.Between || fb1 == PointLinePos.Start || fb1 == PointLinePos.End) return 0;
			if (fb2 == PointLinePos.Between || fb2 == PointLinePos.Start || fb2 == PointLinePos.End) return 0;
			return TestIntersection0(a1, a2, b1, b2);
		} */
		// !!! �������
		public static Point Intersection(Point a1, Point a2, Point b1, Point b2) {
			Point da = a2 - a1;
			Point db = b2 - b1;
			double tu = db.x * (b1.y - a1.y) + db.y * (a1.x - b1.x);
			double td = db.x * da.y - da.x * db.y;
			if (Math.Abs(td) < EPS) return null;
			tu /= td;
			if (tu < -EPS) return null;
			if (tu > 1.0 + EPS) return null;
			double su = da.x * (a1.y - b1.y) + da.y * (b1.x - a1.x);
			double sd = da.x * db.y - db.x * da.y;
			if (Math.Abs(sd) < EPS) return null;
			su /= sd;
			if (su < -EPS) return null;
			if (su > 1.0 + EPS) return null;
			double xa = a1.x + da.x * tu;
			double xb = b1.x + db.x * su;
			double ya = a1.y + da.y * tu;
			double yb = b1.y + db.y * su;
			//if (Math.Abs(xa - xb) > EPS || Math.Abs(ya - yb) > EPS) {
			//	Console.WriteLine("Point.Intersection({0}, {1}, {2}, {3}), t={4}, s={5}, pat=({6}, {7}), pbs=({8}, {9})",
			//		a1, a2, b1, b2, tu, su,	xa, ya, xb, yb);
			//}
			xa = (xa + xb) / 2.0;
			ya = (ya + yb) / 2.0;
			return new Point(xa, ya);
			//throw new NotImplementedException("Point.Intersection not implemented yet");
		}


		// ������������ ����� this ������������ ������� p1p2. ��������� ����������� ��������� EPS
		public PointLinePos PointLinePosition(Point p1, Point p2) {
			if (p1==null || p2==null) return PointLinePos.Outside;
			// ���������� ������ ������� � ������ ���������. ������������ ���������� �����
			double p2x = p2.x - p1.x;
			double p2y = p2.y - p1.y;
			double px = x - p1.x;
			double py = y - p1.y;
			double l = Math.Sqrt(p2x*p2x + p2y*p2y);
			if (l >= EPS) {
				// �������� ������� p1p2 ���, ����� �� ����� �� ��� x. ������������ �������� �����.
				double temp = (p2x * px + p2y * py); // - ��� + ??? ���� -, ����� ����������� ���� ��� ������ �������� ����?
				py = (p2y * px - p2x * py) / l; // - ��� + ???
				px = temp / l;
				// ������ �������� ��������� ����� ������������ ���� � l �� ��� �
				if (py < -EPS) return PointLinePos.Left;
				if (py > EPS) return PointLinePos.Right;
				if (px < -EPS) return PointLinePos.Behind;
				if (px > l+EPS) return PointLinePos.Before;
				if (px > EPS) {
					if (px < l-EPS) return PointLinePos.Between;
					else return PointLinePos.End;
				} else {
					return PointLinePos.Start;
				}
			} else {  // ������� p1p2 ������ EPS - ������� ��� ������
				if (px < -EPS || px > EPS || py < -EPS || py > EPS) return PointLinePos.Contains;
				return PointLinePos.Outside;
			}
		}

		override public string ToString() {
			return "(" + x.ToString() + ", " + y.ToString() + ")";
		}
		virtual public string ToString(string format) {
			return "(" + x.ToString(format) + ", " + y.ToString(format) + ")";
		}
		virtual public string ToJson() {
			return "{ \"x\": " + x.ToString() + ", \"y\":" + y.ToString() + "}";
		}
	}
	/*
	public class Vertex
	{
		public Point p { get; set; }
		public Vertex prev, next;
		public double totalDistance;
	}
	*/
	// ����������� �����, ���� �����. ���� ������������ ������ ��� ����������� ��������������� ��������������.
	// ����� ��������� ��� �����, �� ��� ��������� ��������.
	public class Edge {
		public Point p0;
		public Point p1;
	}
	public class Polygon : List<Point>
	{
		//public List<Point> points;
		private double[] constant = null, multiply = null;
		private bool fPrepared = false;
		static double EPS = 1e-6;

		/*
		// !!! ������
		public void PrepareTest() {
			int i, j = Count - 1;
			constant = new double[Count];
			multiply = new double[Count];
			for (i = 0; i < Count; i++) {
				if (this[j].y == this[i].y) {
					constant[i] = this[i].x;
					multiply[i] = 0;
				} else {
					constant[i] = this[i].x
						- (this[i].y * this[j].x) / (this[j].y - this[i].y)
						+ (this[i].y * this[i].x) / (this[j].y - this[i].y);
					multiply[i] = (this[j].x - this[i].x) / (this[j].y - this[i].y);
				}
				j = i;
			}
			fPrepared = true;
		}
		//public bool TestPoint(Point p) { return TestPoint2(p)<=0; }
		// !!! ������
		public bool TestPoint1(Point p) {
			if (!fPrepared) {
				PrepareTest();
			}
			bool oddNodes = false;
			bool current = false;
			if (Count > 1) current = (this[Count - 1].y > p.y);
			else return false;
			bool previous;
			for (int i = 0; i < Count; i++) {
				previous = current;
				current = (this[i].y > p.y);
				if (current != previous) {
					double d = p.y * multiply[i] + constant[i] - p.x;
					oddNodes ^= d < 0;
				}
			}
			return oddNodes;
		}*/

		// ��������, ����� �� ����� p1 �� �������������� ����, ���������� � p0 ������ (����� ��� �)
		// -1 - ����� ����, +1 - ����� ����, 0 - ����� � �������� ����������� EPS �� ����
		private int TestPointHoriz(Point p0, Point p1) {
			if (p1.y < p0.y - EPS) return -1;
			if (p1.y > p0.y + EPS) return +1;
			return 0;
		}
		private int TestPointVert(Point p0, Point p1) {
			if (p1.x < p0.x - EPS) return -1;
			if (p1.x > p0.x + EPS) return +1;
			return 0;
		}
		// !!! ���������� �� �������������� ������ � �������� �� ���� ������ � �����, ������������ ���������� �� ���� � ��. �������.
		public int TestPoint(Point p) { // <0 - ������ >0 - ������� ==0 - �� �������
			Point pprev = null;
			int i, start=Count-1;
			bool fEdgeOnRay = false;
			bool fZeroY = false;
			bool fBound = false;
			int fcur=0, fprev=0;
			int intersectCount = 0;
			if (Count < 2) return 0;
			//start = 0;
			for (start=0; start<Count; start++) {
				pprev = this[start];
				fprev = TestPointHoriz(p, pprev);
				if (fprev != 0) break;
			}
			//int pstart = start;
			//start++;
			//if (start >= Count) start = 0;
			//for (i=(start+1)%Count; i!=start; ) {
				//if (i==pstart) break;
			i = start;
			do {
				i++;
				if (i >= Count)	i = 0;
				Point pcur = this[i];
					// fZeroY - ���� ��� ��� ���������� ������� ��� �����, ����������� �� Y � �����������
					// fEdgeOnRay - ���� ����, ���� ������� ��� ����� ����� ������ �� ����.
					// ��� ����� ��������������� ��� ����������� ��������� � ������������ ����� ��������� ������ �����,
					// �� ����������� �� Y
					bool fIntersect = false;
					fZeroY = false;
					fcur = TestPointHoriz(p, pcur);
					if ((fprev > 0 && fcur > 0) || (fprev < 0 && fcur < 0)) { // ������� � ���������� ����� � ����� ������� �� Y -
						// - ������������ ����� ���. ���� ���� ����� ���� ���� �������������� ������� �� ����.
						fIntersect = false;
						fEdgeOnRay = false;
						fZeroY = false;
						fBound = false;
						fprev = fcur;
					} else if ((fprev > 0 && fcur < 0) || (fprev < 0 && fcur > 0)) { // �� Y ���� ����, ������ ���� - ��������� X
						if (fEdgeOnRay) { // ����� prev � cur ��� �������, ������� �� ���� - ����������� ����� ����.
							fIntersect = true;
							fBound = false;
						} else if ((pprev.x < p.x - EPS) && (pcur.x < p.x - EPS)) {
							fIntersect = false; // �� � ��� ����� �����
							fBound = false;
							//return 0;
						} else if ((pprev.x > p.x + EPS) && (pcur.x > p.x + EPS)) {
							if (!fZeroY) fIntersect = true; // ����� prev � cur ��� ������ ����� � prev � cur ������ - ����������� ����
							else fIntersect = false; //  ����� fEdgeOnRay==false � ������ ������� ����������� �� Y 
							// ����� ����� - ����������� ���.
							fBound = false;
							//return 0;
						} else { // ���� �����, ������ ������ ��� ���-�� �� ������� - ����� ������ �����������...
							// TODO: ������ ����������� � ����� Y=p.y, X>=p.x
							// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
							double dy = pcur.y - pprev.y;
							double xs;
							if (Math.Abs(dy) > EPS) {
								xs = pprev.x - (pprev.y - p.y) * (pcur.x - pprev.x) / dy;
							} else {
								xs = p.x;
							}
							if (xs < p.x - EPS) { // ��� �����������
								fIntersect = false;
							} else if (xs > p.x + EPS) { // ���� �����������
								fIntersect = true;
							} else {
								fBound = true;
							}
							//return 0; // !!! TEST !!!
						}
						// ������� ����� ���������.
						fEdgeOnRay = false;
						fZeroY = false;
						fprev = fcur;
						pprev = pcur;
					} else { // ���� �� ���� ����
						//return 0;
						if (fprev != 0 && fcur == 0) {
							// ����� ������ �� ������ � fprev �� ������������
							// �� ��������� ������� �, ���� �� ������, �� ����� ��� ����� ����� ����������� ����� ����� 
							// (� ��� �����-����� ���)
							if (fZeroY) { // ������ ���������� ������� ���� ��������� �� Y..
								if (pprev.x < p.x - EPS && pcur.x > p.x + EPS) {
									// ����� �� �������. �����?
									fBound = true;
								} else if (pprev.x > p.x + EPS && pcur.x > p.x + EPS) {
									// ����� ����� �� ����. ������ �� ������, ������� ���������� ���������. 
									// ��� ��������� ����, ��� ����� X ����� �� ���������.
									fEdgeOnRay = true;
								} else if (pprev.x < p.x - EPS && pcur.x < p.x - EPS) {
									// ����������� _�����_ ���, �� ����� ������������ ���������� � ����������� �������. 
									// ������ �� ������.
								} else {
									// ������ ��� ����� - ����� �� ������� - �����?
									fBound = true;
								}
							} else {
								fZeroY = true;
								if (pcur.x > p.x + EPS) {
									//fEdgeOnRay = true;
								} else if (pcur.x < p.x - EPS) { // pcur ����� - ����������� ����� ��� - ������ �� ������
								} else { // ����� �� ������� - ������ �� ������ ��� ������� 0 ?
									fBound = true;
								}
							}
						} else if (fprev == 0) {
							// ������ ���� �� ������ �������, ������ ��� �� �� ��������� ������� fprev
							// � ��� ���� ����� ����������(���������) ����� ��������� �� ���?
							// ����� ���� ����� ����� ������ ������ �� ����� ������������� � ����� ��� ������ ��������� �������?
							// ��� ���������� �����? �����������, ������� ���������� �� ���� - ��� ������?
							throw new Exception(
								string.Format("fprev=={1} && fcur=={2}, � ������� fprev �� ���������, ��� �� �����?",
												fprev, fcur));
						} else {
							throw new Exception(string.Format("fprev=={1} && fcur=={2}, something strange happend...", fprev, fcur));
						}
					}
					if (fBound) return 0;
					if (fIntersect) intersectCount++;
					fIntersect = false;
					fBound = false;
					if (!fZeroY) {
						//	pprev = pcur;
						//fprev = fcur;
					}
					pprev = pcur; // ��� �����, ��� �������� ����� ���� ���������� �����, ���� ���������� ���������? (����� �� �������?)
				} while (i != start);
			if ((intersectCount & 1) != 0) return -1; // ������
			return 1; // �������
		}

		/*
		//  This function should be called with the full set of *all* relevant polygons.
		//  (The algorithm automatically knows that enclosed polygons are �no-go�
		//  areas.)
		//
		//  Note:  As much as possible, this algorithm tries to return YES when the
		//         test line-segment is exactly on the border of the polygon, particularly
		//         if the test line-segment *is* a side of a polygon.

		// !!! ��� ���� ���� ����������, ������ �� ��������.
		public bool TestLine1(Point start, Point end) {
			double theCos, theSin, dist, sX, sY, eX, eY, rotSX, rotSY, rotEX, rotEY, crossX;
			int i, j;

			end = end - start;
			dist = end.r;
			theCos = end.x / dist;
			theSin = end.y / dist;

			for (i = 0; i < Count; i++) {
				j = i + 1;
				if (j == Count) j = 0;

				sX = this[i].x - start.x;
				sY = this[i].y - start.y;
				eX = this[j].x - start.x;
				eY = this[j].y - start.y;

				// in original code there was a error in end comparisons
				if ((Math.Abs(sX) < EPS && Math.Abs(sY) < EPS && Math.Abs(this[j].x - end.x) < EPS && Math.Abs(this[j].y - end.y) < EPS)
				|| (Math.Abs(eX) < EPS && Math.Abs(eY) < EPS && Math.Abs(this[i].x-end.x)<EPS && Math.Abs(this[i].y-end.y)<EPS)) {
					return true;
				}

				rotSX = sX * theCos + sY * theSin;
				rotSY = sY * theCos - sX * theSin;
				rotEX = eX * theCos + eY * theSin;
				rotEY = eY * theCos - eX * theSin;
				if (rotSY < 0.0 && rotEY > 0.0
				|| rotEY < 0.0 && rotSY > 0.0) {
					crossX = rotSX + (rotEX - rotSX) * (0.0 - rotSY) / (rotEY - rotSY);
					if (crossX >= 0.0+EPS && crossX <= dist-EPS) return false;
				}

				if (rotSY == 0.0 && rotEY == 0.0
				&& (rotSX >= 0.0 || rotEX >= 0.0)
				&& (rotSX <= dist || rotEX <= dist)
				&& (rotSX < 0.0 || rotEX < 0.0
				|| rotSX > dist || rotEX > dist)) {
					return false;
				}
			}
			return TestPoint(start + end * 0.5) <= 0;
		}
		*/
		/*public bool TestLine2(Point start, Point end) {
			Point pprev = null;
			int i, istart=Count-1;
			int fcur=0, fprev=0;
			int intersectCount = 0;
			if (Count < 2) return false;
			pprev = this[istart];
			istart = 0;
			i = istart;
			do {
				i++;
				if (i >= Count)	i = 0;
				Point pcur = this[i];
				//Point result = Point.Intersection(start, end, pcur, pprev);
				//if (result != null)
				//	return false;
				if (Point.TestIntersection2(start, end, pcur, pprev) > 0) return false;
				pprev = pcur;
			} while (i != istart);
			return true; // true = ������ ���� �� ���������� �������
		}*/

		// +2 - ��� ����� ������� ������� (�������� ���-�� ����������, �� �����������)
		// -2 - ������� ������� ������ � ������ �� ����������
		// 0 - ������� �� �������, ����������� ���
		// -1 - ���� �� ������ ������, ������ �� �������, ����������� ���, �������� ������� �������
		// +1 - ���� �� ������ �������, ������ �� �������, ����������� ���, �������� ������� �������
		// +3 - ������� ���������� �������
		int test_reclevel = 0;
		public int TestLine3(Point pstart, Point pend) {
			Point pprev = null;
			int i, istart = Count - 1;
			int fstart = 0, fend = 0, fmid = 0;
			if (Count < 2) return 0;
			pprev = this[istart];
			fstart = TestPoint(pstart);
			fend = TestPoint(pend);
			string recstr = test_reclevel.ToString();
			for (int j=0; j<test_reclevel; j++) recstr += "  ";
			//Console.WriteLine("{0} TestLine3({1}, {2}), fstart={3}, fend={4}", recstr, pstart, pend, fstart, fend);
			if (fstart > 0 && fend > 0) return +2; // ��� ����� ������� �������
			if ((fstart > 0 && fend < 0) || (fstart < 0 && fend > 0)) return +3;
			// ��� ���� �� ���� ���� ������������� - ����� ������� ��� �� ������� ��� ������. �������� ��� ���� ����� �� �������, � ������ �������.
			// �������� �������� ������� - ���� ��� �������, �� ���� ��� � ������� ��� ������������.
			Point phalf = pstart + (pend - pstart) * 0.5;
			int fhalf = TestPoint(phalf);
			//Console.WriteLine("{0}   fhalf={1}", recstr, fhalf);
			if (fhalf > 0 && (fstart < 0 || fend < 0)) return +3;
			if (fhalf > 0 && fstart==0 && fend==0) return +3; // ��� ��������, ����� ������� ��������� ��� ������� � ����� ���� ���������� � ���� ���. ���������� ����� �� ����.
			// ������ ��������� �� ����������� �� ��������� ��������������
			PointLinePos fprev = pprev.PointLinePosition(pstart, pend);
			i = istart;
			//Console.WriteLine("{0}   LOOP", recstr);
			do {
				i++;
				if (i >= Count) i = 0;
				Point pcur = this[i];
				PointLinePos fcur = pcur.PointLinePosition(pstart, pend); // �����
				PointLinePos fs = pstart.PointLinePosition(pprev, pcur);
				PointLinePos fe = pend.PointLinePosition(pprev, pcur);
				//Console.WriteLine("{0} {1}  test lines: prev={2}, cur={3}, start={4}, end={5}", recstr, i, pprev, pcur, pstart, pend);
				//Console.WriteLine("{0} {1}  position codes: prev={2}, cur={3}, start={4}, end={5}", recstr, i, fprev, fcur, fs, fe);
				if ((fcur == PointLinePos.Left && fprev == PointLinePos.Right)
				|| (fcur == PointLinePos.Right && fprev == PointLinePos.Left)) { // �������� �����������
					if ((fs == PointLinePos.Left && fe == PointLinePos.Right)
					|| (fs == PointLinePos.Right && fe == PointLinePos.Left)) { // ����������� ����� ����
						return +3;
					}
				}
				//if ((fcur == PointLinePos.Between) || fcur==PointLinePos.Start || fcur==PointLinePos.End)) {
				//|| (fs == PointLinePos.Between || fs==PointLinePos.Start || fs==PointLinePos.End)
				//|| (fe == PointLinePos.Between || fe==PointLinePos.Start || fe==PointLinePos.End)) { // ���� �������
				// �� ���� ��������� ��������� ������ ������, ������ ��� �� ��������� ��� ������� ������� ������ �������
				if ((fcur == PointLinePos.Between)) { // ���� �������
					Point pmid = Point.Intersection(pstart, pend, pcur, pprev); // ����� ����� �������
					if (pmid != null) { // ����������� ����, �� �������� ��� �������
						fmid = TestPoint(pmid); // ������ ���� ������ 0!!!
						//if (pstart.Distance(pmid) > EPS && pend.Distance(pmid) > EPS) {
							test_reclevel++;
							int res_start = TestLine3(pstart, pmid);
							int res_end = TestLine3(pmid, pend);
							test_reclevel--;
							//Console.WriteLine("{0}   pmid={1}, fmid={2}, res_start={3}, res_end={4}", 
							//	recstr, pmid, fmid, res_start, res_end);
							if (res_start >= 2 || res_end >= 2) return Math.Max(res_start, res_end); // ���� �����������
						//}
					}
				}
				pprev = pcur;
				fprev = fcur;
			} while (i != istart);
			//Console.WriteLine("{0}   ENDLOOP", recstr);
			if ((fstart == 0 && fend > 0) || (fend == 0 && fstart > 0)) return +1;
			if ((fstart == 0 && fend < 0) || (fend == 0 && fstart < 0)) return -1;
			if (fstart < 0 && fend < 0) return -2;
			if (fstart == 0 && fend == 0) return 0;
			return 0;
		}

		public bool TestLine(Point p1, Point p2) {
			int rc = TestLine3(p1, p2);
			//Console.WriteLine("TestLine({0}, {1}) returns {2}", p1, p2, rc);
			if (rc>= 2) return false;
			else return true;
		}
		// ��������� � ����� p2 ����������� ������� p1p2 �� �������� ��������������.
		// ������� �������� - ���� ����������� ����� �� 1 (� 0 ��� �����) - ���������� null
		// if (icnt==1) return min_point; else return null;
		// �����������:
		// - ������� ����������� � ������ � ����� - �� ���� ���� ��� ���
		// - ������� 3 �����������, ���������� ��������� (� ������ null)
		// - ��� ������, ���� ���� (��� ���) ����� �� �������?
		// - ��� ������ ���� ��� ����� ������ ?
		public Point OneIntersection(Point p1, Point p2) {
			if (Count < 2) return null;
			if (p1 == null || p2 == null) return null;
			Point p = null;
			Point min_point = null;
			double min_distance = Double.PositiveInfinity;
			int icnt = 0;
			//Console.WriteLine("OneIntersection({0}, {1}", p1, p2);
			Point p3 = this[Count-1];
			double d = Double.NaN;
			foreach (Point p4 in this) {
				d = Double.NaN;
				p = Point.Intersection(p1, p2, p3, p4);
				if (p != null) {
					icnt++;
					d = p2.Distance(p);
					if (d<min_distance) {
						min_distance = d;
						min_point = p;
					}
				}
				//Console.WriteLine("p3={0}, p4={1}, intersection={2}, dist={3}, min={4}", p3, p4, p, d, min_distance);
				p3 = p4;
			}
			if (icnt==1) return min_point;
			else return null;
		}
		public Point NearestIntersection(Point p1, Point p2) {
			if (Count < 2) return null;
			if (p1 == null || p2 == null) return null;
			Point p = null;
			Point min_point = null;
			double min_distance = Double.PositiveInfinity;
			int icnt = 0;
			//Console.WriteLine("OneIntersection({0}, {1}", p1, p2);
			Point p3 = this[Count-1];
			double d = Double.NaN;
			foreach (Point p4 in this) {
				d = Double.NaN;
				p = Point.Intersection(p1, p2, p3, p4);
				if (p != null) {
					icnt++;
					d = p2.Distance(p);
					if (d<min_distance) {
						min_distance = d;
						min_point = p;
					}
				}
				//Console.WriteLine("p3={0}, p4={1}, intersection={2}, dist={3}, min={4}", p3, p4, p, d, min_distance);
				p3 = p4;
			}
			//if (icnt>=1) return min_point;
			//else return null;
			return min_point;
		}

		public Edge RectBounds() {
			if (Count<1) return null;
			Point pmin = new Point(this[0]);
			Point pmax = new Point(this[0]);
			foreach (Point p in this) {
				if (pmin.x < p.x) pmin.x = p.x;
				if (pmin.y < p.y) pmin.y = p.y;
				if (pmax.x > p.x) pmax.x = p.x;
				if (pmax.y > p.y) pmax.y = p.y;
			}
			return new Edge() { p0=pmin, p1=pmax };
		}
		public Point MinBound { get {
				if (Count<1) return null;
				Point pmin = new Point(this[0]);
				foreach (Point p in this) {
					if (pmin.x < p.x) pmin.x = p.x;
					if (pmin.y < p.y) pmin.y = p.y;
				}
				return pmin;
			}
		}
		public Point MaxBound {	get {
				if (Count<1) return null;
				Point pmax = new Point(this[0]);
				foreach (Point p in this) {
					if (pmax.x>p.x) pmax.x = p.x;
					if (pmax.y>p.y) pmax.y = p.y;
				}
				return pmax;
			}
		}
	// ���� �� ��� �������������� ��������� ������� � ����� p.
	// ���������� ���������� ����� �� ���� �������, ���� ���� ������������� �� p
	// ����� ��������� �������, ���������� ���������� ����� ���� �� ������� � �� �� ��������������.
	public Point NearestIntersection(Point p) {
			if (p==null) return null;
			if (Count < 2) return null;
			Point p1 = this[Count-1];
			Point min_perp = p1;
			double min_dist = p.Distance(p1);
			foreach (Point p2 in this) {
				if (p1!=null) {
					Point perp = p.Perpendicular(p1, p2);
					double dist = Double.PositiveInfinity;
					if (perp != null) {
						dist = p.Distance(perp);
						if (dist<min_dist || min_dist<0) {
							min_dist = dist;
							min_perp = perp;
						}
					}
					dist = p.Distance(p2);
					if (dist < min_dist || min_dist < 0) {
						min_dist = dist;
						min_perp = p2;
					}
				}
				p1 = p2;
			}
			return min_perp;
		}

	// ���������� �� C#, � ������� �� ���������:
	// ������� ���������� ���� �� start � end ������ �������� (this)
	// ���������� ������ ����� ����, ������� ������ � �����.
	// ���� ���� �� ����������, ���������� null

		public List<Point> FindPath(Point start, Point end, double MaxOutsideStartDistance=-1, double MaxOutsideEndDistance=-1, bool fAllowOutsideEnd=false) {

			List<Point> solution = new List<Point>();
			Point[] pointList = new Point[Count + 10];
			double[] totalDist = new double[Count + 10];
			int[] prev = new int[Count + 10];
			int cap = 0;
			Point pathStart = start;
			Point pathEnd = end;
			bool fStartOutside = false;
			bool fEndOutside = false;

			int pointCount, treeCount, polyI, i, j, bestI, bestJ;
			double bestDist;

			//Console.WriteLine("FindPath({0}, {1}, {2}, {3}, {4})", 
			//	start, end, MaxOutsideStartDistance, MaxOutsideEndDistance, fAllowOutsideEnd);
			//Console.WriteLine("Bounds={0}", this.ToString());

			if (TestPoint(start)>0) {
				fStartOutside = true;
				if (MaxOutsideStartDistance > 0) {
					pathStart = NearestIntersection(start);
					//Console.WriteLine("StartIntersection={0}", pathStart);
					if (pathStart != null && Math.Abs(start.Distance(pathStart)) < MaxOutsideStartDistance) {
						cap++;
					} else {
						return null;
					}
				} else {
					return null;
				}
			} else {
				cap++;
			}
			if (TestPoint(end)>0) {
				if (fAllowOutsideEnd) {
					fEndOutside = true;
				} else {
					return null;
				}
			}

			if (TestLine(pathStart, end)) {
				solution.Capacity = cap + 1;
				solution.Add(start);
				if (pathStart != start) solution.Add(pathStart);
				solution.Add(end);
				return solution;
			}
			//  Build a point list that refers to the corners of the
			//  polygons, as well as to the startpoint and endpoint.
			pointList[0] = pathEnd; // start;
			pointCount = 1;
			for (i = 0; i < Count; i++) {
				pointList[pointCount] = this[i];
				//totalDist[pointCount] = Double.Infinity;
				pointCount++;
			}
			pointList[pointCount] = pathStart; // end;
			pointCount++;
			//  Initialize the shortest-path tree to include just the startpoint.
			treeCount = 1; totalDist[0] = 0.0;
			//  Iteratively grow the shortest-path tree until it reaches the endpoint
			//  -- or until it becomes unable to grow, in which case exit with failure.
			bestJ = 0; bestI = -1;
			Point lastIns = null;
			while (bestJ < pointCount - 1) {
				bestDist = Double.PositiveInfinity; // You really knew that (Double.NaN == Double.NaN) == false ?
				for (i = 0; i < treeCount; i++) {
					for (j = treeCount; j < pointCount; j++) {
						double newDist = Double.PositiveInfinity;
						Point ins = null;
						//Console.WriteLine("TestLine({0}, {1})", i, j);
						//if (fEndOutside && (pointList[j] == pathEnd || pointList[i] == pathEnd)) {
						//}
						if (TestLine(pointList[i], pointList[j])) {
							newDist = totalDist[i] + pointList[i].Distance(pointList[j]);
							//Console.WriteLine("distance({0}, {1})={2}, newDist={3}", 
							//	i, j, pointList[i].Distance(pointList[j]), newDist);
						} else {
							//Console.WriteLine("NO PATH");
							if (fEndOutside) {
								if (pointList[j] == pathEnd) {
									ins = OneIntersection(pointList[i], pointList[j]);
									//Console.WriteLine("OneIntersection(i, j)={0}", ins);
									if (ins != null) {
										if (pathEnd.Distance(ins) < MaxOutsideEndDistance) {
											newDist = totalDist[i] + pointList[i].Distance(pointList[j]);
										}
									}
								} else if (pointList[i] == pathEnd) {
									ins = OneIntersection(pointList[j], pointList[i]);
									//Console.WriteLine("OneIntersection(j, i)={0}", ins);
									if (ins != null) {
										if (pathEnd.Distance(ins) < MaxOutsideEndDistance) {
											newDist = totalDist[i] + pointList[i].Distance(pointList[j]);
										}
									}
								}
							}
						}
						if (newDist < bestDist) {
							bestDist = newDist;
							bestI = i; bestJ = j;
							lastIns = ins;
						}
					}
				}
				if (bestDist == Double.PositiveInfinity) return null;   //  (no solution)
				prev[bestJ] = bestI;
				totalDist[bestJ] = bestDist;
				// swap [bestJ] and [treeCount] pointList items
				Point tmp = pointList[bestJ]; pointList[bestJ] = pointList[treeCount]; pointList[treeCount] = tmp;
				// also need to swap totalDist and prev
				totalDist[bestJ] = totalDist[treeCount]; totalDist[treeCount] = bestDist;
				prev[bestJ] = prev[treeCount]; prev[treeCount] = bestI;
				treeCount++;
			}
			//  Load the solution arrays.
			int solutionNodes = 0;
			i = treeCount - 1;
			while (i > 0) {
				i = prev[i]; //pointList[i].prev; 
				solutionNodes++;
			}
			j = solutionNodes;
			i = treeCount - 1;
			solution.Capacity = solutionNodes + cap - 1; // pre-allocate array memory
			if (pathStart != start) solution.Add(start);
			while (j >= 0) {
				solution.Add(pointList[i]);
				i = prev[i];
				j--;
			}
			if (fEndOutside && lastIns != null) { // ������� ���������� ��������
				solution[solution.Count-1] = lastIns;
			}
			if (pathEnd != end) solution.Add(end);
			//  Success.
			return solution;
		}
		// ������ ������ �����...
		public override string ToString() {
			string s = "[ ";
			System.Globalization.NumberFormatInfo fmt = (System.Globalization.NumberFormatInfo)System.Globalization.NumberFormatInfo.CurrentInfo.Clone();
			fmt.NumberDecimalSeparator = ".";
			fmt.NumberNegativePattern=1;
			foreach (Point p in this) {
				s += "{"+p.x.ToString(fmt)+", "+p.y.ToString(fmt)+"}, ";
			}
			s = s.TrimEnd(',', ' ') + " ]";
			return s;
		}
		public virtual string ToJson() {
			string s = "[ ";
			System.Globalization.NumberFormatInfo fmt = (System.Globalization.NumberFormatInfo)System.Globalization.NumberFormatInfo.CurrentInfo.Clone();
			fmt.NumberDecimalSeparator = ".";
			fmt.NumberNegativePattern=1;
			foreach (Point p in this) {
				s += "{ \"Ra\": "+p.x.ToString(fmt)+", \"Dec\": "+p.y.ToString(fmt)+"}, ";
			}
			s = s.TrimEnd(',', ' ') + " ]";
			return s;
		}
		public virtual bool TryParse(string s) {
			if (string.IsNullOrWhiteSpace(s)) return false;
			char[] c = s.ToCharArray();
			int state = 0;
			int pointBegin=-1, pointEnd=-1, commaPos=-1;
			bool fInPoint=false, fSetX=false, fSetY=false, fExit = false;
			List<Point> newList = new List<Point>(16);
			double x=double.NaN, y=double.NaN;
			int pointCount = 0;
			for (int i = 0; i < c.Length; i++) {
				switch (c[i]) {
					case '[':
						break;
					case ']':
						fExit = true;
						break;
					case '{':
						fInPoint = true;
						pointBegin = i;
						fSetX = fSetY = false;
						commaPos = -1;
						break;
					case '}':
						if (fInPoint) {
							if (commaPos > 0) {
								string ps = s.Substring(commaPos + 1, i - commaPos - 1);
								if (ps != null) {
									if (Double.TryParse(ps, out y)) fSetY = true;
									if (Double.TryParse(
										ps.Replace(".", System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator), 
										out y)) fSetY = true;
								}
								if (fSetX && fSetY) {
									Point p = new Point(x, y);
									newList.Add(p);
									pointCount++;
								}
							}
							fInPoint = false;
						};
						fInPoint = false;
						fSetX = fSetY = false;
						commaPos = -1;
						pointBegin = -1;
						break;
					case ',':
					case ';':
						if (fInPoint || pointCount==0) {
							string ps = null;
							if (i>1 && pointBegin<=0) ps = s.Substring(0, i - 1);
							else if (i>1 && pointBegin<i) ps = s.Substring(pointBegin + 1, i - pointBegin - 1);
							if (ps != null) {
								if (Double.TryParse(ps, out x)) fSetX = true;
								if (Double.TryParse(ps.Replace(".", System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator), out x)) fSetX = true;
							}
							commaPos = i;
						}
						break;
				}
				if (fExit) break;
			}
			if (commaPos > 0 && pointCount==0) {
				string ps = s.Substring(commaPos + 1, c.Length - commaPos - 1);
				if (Double.TryParse(ps, out y)) fSetY = true;
				if (fSetX && fSetY) {
					Point p = new Point(x, y);
					newList.Add(p);
					pointCount++;
				}
			}

			if (newList.Count >= 3) {
				this.Clear();
				this.AddRange(newList);
				fPrepared = false;
				return true;
			} else {
				return false;
			}
		}
		public bool Test() {
			TestParse();
			List<Point> path = FindPath(new Point(3.34, -2.68), new Point(2.02, 4.0));
			if (path != null) { 
			// ������ ���������� ����� ����:
			// Path[0]=(3.34, -2.68)
			// Path[1]=(0, 0)
			// Path[2]=(-4.5, -2.5)
			// Path[3]=(2.02, 4)
				string test_path = "[ {3.34, -2.68}, {0, 0}, {-4.5, -2.5}, {2.02, 4} ]";
				Polygon p = new Polygon();
				p.AddRange(path);
				string testOut = p.ToString();
				if (test_path.Trim().ToUpperInvariant().Equals(testOut.Trim().ToUpperInvariant())) 
					return true;
			}
			return false;
		}
		public bool TestParse() {
			string testIn = "[ {-5, -5}, {0, 0}, {5.1, -5.6}, {4.5, -2.5}, {0.5, 1.5}, {-4.5, -2.5}, {4.5, 5.1}, {-4.5, 3.5}, {-5.3, 5} ]";
			bool rc = TryParse(testIn);
			if (rc) {
				string testOut = ToString();
				if (testIn.Trim().ToUpperInvariant().Equals(testOut.Trim().ToUpperInvariant())) 
					return true;
			}
			return false;
		}
		public bool TestPointLinePos() {
			double eps1 = 1e-7;
			double eps2 = 1e-3;
			Point line0 = new Point(-5, 0);
			Point line1 = new Point(5, 0);
			List<Point> tp = new List<Point>();
			List<PointLinePos> rr = new List<PointLinePos>(); // ���������� ���������
			List<PointLinePos> rc = new List<PointLinePos>();
			tp.Add(line0); rr.Add(PointLinePos.Start); rc.Add(PointLinePos.Outside);
			tp.Add(line1); rr.Add(PointLinePos.End); rc.Add(PointLinePos.Outside);

			tp.Add(new Point(0, -2 * eps2)); rr.Add(PointLinePos.Right); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(-5, -1)); rr.Add(PointLinePos.Right); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(5, -1)); rr.Add(PointLinePos.Right); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(-6, -1)); rr.Add(PointLinePos.Right); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(6, -1)); rr.Add(PointLinePos.Right); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(0, -0.5 * eps1)); rr.Add(PointLinePos.Between); rc.Add(PointLinePos.Outside);

			tp.Add(new Point(0, +0.5 * eps1)); rr.Add(PointLinePos.Between); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(0, +2 * eps2)); rr.Add(PointLinePos.Left); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(-5, +1)); rr.Add(PointLinePos.Left); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(5, +1)); rr.Add(PointLinePos.Left); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(-6, +1)); rr.Add(PointLinePos.Left); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(6, +1)); rr.Add(PointLinePos.Left); rc.Add(PointLinePos.Outside);

			tp.Add(new Point(-5 + 0.5 * eps1, +0.5 * eps1)); rr.Add(PointLinePos.Start); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(-5 - 0.5 * eps1, -0.5 * eps1)); rr.Add(PointLinePos.Start); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(+5 + 0.5 * eps1, +0.5 * eps1)); rr.Add(PointLinePos.End); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(+5 - 0.5 * eps1, -0.5 * eps1)); rr.Add(PointLinePos.End); rc.Add(PointLinePos.Outside);

			tp.Add(new Point(-5 - 2 * eps2, +0.5 * eps1)); rr.Add(PointLinePos.Behind); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(-5 - 2 * eps2, -0.5 * eps1)); rr.Add(PointLinePos.Behind); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(+5 + 2 * eps2, +0.5 * eps1)); rr.Add(PointLinePos.Before); rc.Add(PointLinePos.Outside);
			tp.Add(new Point(+5 + 2 * eps2, -0.5 * eps1)); rr.Add(PointLinePos.Before); rc.Add(PointLinePos.Outside);

			List<int> errors = new List<int>();
			Random rnd1 = new Random();
			for (int rrr=0; rrr<100; rrr++) {
				double offsetX = (rnd1.NextDouble()-0.5)*10;
				double offsetY = (rnd1.NextDouble()-0.5)*10;
				double rot = (rnd1.NextDouble()-0.5)*Math.PI*4;
				//offsetX = 0;
				//offsetY = 0;
				//rot = Math.PI / 8.0*(rrr+1);
				Console.WriteLine("*** [{0}] *** {1} -> {2} ***", rrr, line0, line1);
				for(int i=2; i<tp.Count; i++) {
					Point p = tp[i];
					rc[i] = p.PointLinePosition(line0, line1);
					if (rc[i] != rr[i]) {
						errors.Add(i);
						Console.WriteLine("Err[{0}]: line={1}->{2}, p={3}, rc={4}, should be {5}", 
							i, line0, line1, p, rc[i], rr[i]);
						rc[i] = p.PointLinePosition(line0, line1);
					}
				}
				for (int i = 0; i < tp.Count; i++) {
					double tempX = tp[i].x * Math.Cos(rot) - tp[i].y * Math.Sin(rot) + offsetX;
					double tempY = tp[i].x * Math.Sin(rot) + tp[i].y * Math.Cos(rot) + offsetY;
					tp[i].x = tempX; tp[i].y = tempY;
				}
			}
			return true;
		}
	} // class Polygon
} // namespace

// Parts of original C code follows...

//  (This function automatically knows that enclosed polygons are "no-go"
//  areas.)
/*
boolean pointInPolygonSet(double testX, double testY, polySet allPolys) {

  bool  oddNodes=NO ;
  int   polyI, i, j ;

  for (polyI=0; polyI<allPolys.count; polyI++) {
    for (i=0;    i< allPolys.poly[polyI].corners; i++) {
      j=i+1; if (j==allPolys.poly[polyI].corners) j=0;
      if   ( allPolys.poly[polyI].y[i]< testY
      &&     allPolys.poly[polyI].y[j]>=testY
      ||     allPolys.poly[polyI].y[j]< testY
      &&     allPolys.poly[polyI].y[i]>=testY) {
        if ( allPolys.poly[polyI].x[i]+(testY-allPolys.poly[polyI].y[i])
        /   (allPolys.poly[polyI].y[j]       -allPolys.poly[polyI].y[i])
        *   (allPolys.poly[polyI].x[j]       -allPolys.poly[polyI].x[i])<testX) {
          oddNodes=!oddNodes; }}}}

  return oddNodes; }



bool lineInPolygonSet(
double testSX, double testSY, double testEX, double testEY, polySet allPolys) {

  double  theCos, theSin, dist, sX, sY, eX, eY, rotSX, rotSY, rotEX, rotEY, crossX ;
  int     i, j, polyI ;

  testEX-=testSX;
  testEY-=testSY; 
dist=sqrt(testEX*testEX+testEY*testEY);
  theCos =testEX/ dist;
  theSin =testEY/ dist;

  for (polyI=0; polyI<allPolys.count; polyI++) {
    for (i=0;    i< allPolys.poly[polyI].corners; i++) {
      j=i+1; if (j==allPolys.poly[polyI].corners) j=0;

      sX=allPolys.poly[polyI].x[i]-testSX;
      sY=allPolys.poly[polyI].y[i]-testSY;
      eX=allPolys.poly[polyI].x[j]-testSX;
      eY=allPolys.poly[polyI].y[j]-testSY;
      if (sX==0. && sY==0. && eX==testEX && eY==testEY
      ||  eX==0. && eY==0. && sX==testEX && sY==testEY) {
        return YES; }

      rotSX=sX*theCos+sY*theSin;
      rotSY=sY*theCos-sX*theSin;
      rotEX=eX*theCos+eY*theSin;
      rotEY=eY*theCos-eX*theSin;
      if (rotSY<0. && rotEY>0.
      ||  rotEY<0. && rotSY>0.) {
        crossX=rotSX+(rotEX-rotSX)*(0.-rotSY)/(rotEY-rotSY);
        if (crossX>=0. && crossX<=dist) return NO; }

      if ( rotSY==0.   && rotEY==0.
      &&  (rotSX>=0.   || rotEX>=0.  )
      &&  (rotSX<=dist || rotEX<=dist)
      &&  (rotSX< 0.   || rotEX< 0.
      ||   rotSX> dist || rotEX> dist)) {
        return NO; }}}

  return pointInPolygonSet(testSX+testEX/2.,testSY+testEY/2.,allPolys); }



double calcDist(double sX, double sY, double eX, double eY) {
  eX-=sX; eY-=sY; return sqrt(eX*eX+eY*eY); }



void swapPoints(point *a, point *b) {
  point swap=*a; *a=*b; *b=swap; }

*/

/*
bool shortestPath(double sX, double sY, double eX, double eY, polySet allPolys,
double *solutionX, double *solutionY, int *solutionNodes) {

  #define  INF  9999999.     //  (larger than total solution dist could ever be)

  point  pointList[1000] ;   //  (enough for all polycorners plus two)
  int    pointCount      ;

  int     treeCount, polyI, i, j, bestI, bestJ ;
  double  bestDist, newDist ;

  //  Fail if either the startpoint or endpoint is outside the polygon set.
  if (!pointInPolygonSet(sX,sY,allPolys)
  ||  !pointInPolygonSet(eX,eY,allPolys)) {
    return NO; }

  //  If there is a straight-line solution, return with it immediately.
  if (lineInPolygonSet(sX,sY,eX,eY,allPolys)) {
    (*solutionNodes)=0; return YES; }

  //  Build a point list that refers to the corners of the
  //  polygons, as well as to the startpoint and endpoint.
  pointList[0].x=sX;
  pointList[0].y=sY; pointCount=1;
  for (polyI=0; polyI<allPolys.count; polyI++) {
    for (i=0; i<allPolys.poly[polyI].corners; i++) {
      pointList[pointCount].x=allPolys.poly[polyI].x[i];
      pointList[pointCount].y=allPolys.poly[polyI].y[i]; pointCount++; }}
  pointList[pointCount].x=eX;
  pointList[pointCount].y=eY; pointCount++;

  //  Initialize the shortest-path tree to include just the startpoint.
  treeCount=1; pointList[0].totalDist=0.;

  //  Iteratively grow the shortest-path tree until it reaches the endpoint
  //  -- or until it becomes unable to grow, in which case exit with failure.
  bestJ=0;
  while (bestJ<pointcount-1) {
    bestDist=INF;
    for (i=0; i<treeCount; i++) {
      for (j=treeCount; j<pointCount; j++) {
        if (lineInPolygonSet(
        pointList[i].x,pointList[i].y,
        pointList[j].x,pointList[j].y,allPolys)) {
          newDist=pointList[i].totalDist+calcDist(
          pointList[i].x,pointList[i].y,
          pointList[j].x,pointList[j].y);
          if (newDist<bestDist) {
            bestDist=newDist; bestI=i; bestJ=j; }}}}
    if (bestDist==INF) return NO;   //  (no solution)
    pointList[bestJ].prev     =bestI   ;
    pointList[bestJ].totalDist=bestDist;
    swapPoints(&pointList[bestJ],&pointList[treeCount]); treeCount++; }

  //  Load the solution arrays.
  (*solutionNodes)= -1; i=treeCount-1;
  while (i> 0) {
    i=pointList[i].prev; (*solutionNodes)++; }
  j=(*solutionNodes)-1; i=treeCount-1;
  while (j>=0) {
    i=pointList[i].prev;
    solutionX[j]=pointList[i].x;
    solutionY[j]=pointList[i].y; j--; }

  //  Success.
  return YES; }


/*
bool pointInPolygon() {

  int   i, j=polyCorners-1 ;
  bool  oddNodes=NO      ;

  for (i=0; i<polyCorners; i++) {
    if ((polyY[i]< y && polyY[j]>=y
    ||   polyY[j]< y && polyY[i]>=y)) {
      oddNodes^=(y*multiple[i]+constant[i]<x); }
    j=i; }

  return oddNodes; }


This is a pretty smart optimization provided to me by Evgueni Tcherniaev:

bool pointInPolygon() {

  bool oddNodes=NO, current=polY[polyCorners-1]>y, previous;
  for (int i=0; i<polyCorners; i++) {
    previous=current; current=polyY[i]>y; if (current!=previous) oddNodes^=y*multiple[i]+constant[i]<x; }
  return oddNodes; }

}
*/